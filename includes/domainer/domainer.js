    /*    <tr class="more">
                <td colspan="4"><a href="#" class="btn-special btn-primary">Ver más</a></td>
            </tr>
    */

$(document).ready(function(){
    if (typeof variable !== 'undefined') {
        
	$tableMain = $('.results-table-main-ajax tbody');
	
	$tablePrefix = $('.results-table-prefix-ajax tbody');
	$tableExtension = $('.results-table-extension-ajax tbody');
	$tableColoquial = $('.results-table-coloquial-ajax tbody');
	
	if($tableMain && keyword){	
		$tableMain.html(`<img src="${template_url}/includes/domainer/loading.gif" width="20%" > cargando ...`);

		 	$tableExtension.html(`<img src="${template_url}/includes/domainer/loading.gif" width="20%" > cargando ...`);

	    jQuery.ajax({
	        type:"POST",
	        url: site_url+"/wp-admin/admin-ajax.php",
	        data: {"action":"searchDomainExtension","keyword":keyword},
	        success:function(data){
	        	$tableMain.html('');
	        	$.each(data, function(i,item){		
	        		console.log(item.domain);        	
		        	row = `<tr>
                <td class="domain-name">${item.domain}<i class="fa fa-check"></i>
                </td>
                <td class="price">`;
                	if(item.show_igv){
                		row += '<span class="igv">IGV incluido*</span>';
                	}
                	if(item.has_offer){
                		row +='<span class="oferta">Oferta</span>';
                	}
                
                	row += `<span class="moneda">${item.currency_simbol}</span><span class="precio">${item.price}</span>`;

            		if(item.has_offer){
            			row += `<p class="price-before">Precio regular ${item.currency_simbol}
                        <span class="tachado">${item.regular_price}</span>
                        </p>`;
            		}
                
                	row += `</td>
                <td class="plazo">x
                    <div class="time-limit dropdown"><a data-toggle="dropdown" class="dropdown-toggle"><span class="time">1 año  </span><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">2 años</a></li>
                            <li><a href="#">3 años</a></li>
                            <li><a href="#">4 años</a></li>
                            <li><a href="#">5 años</a></li>
                        </ul>
                    </div>
                </td>
                <td class="add"><a href="#" class="btn-special btn-default">Agregar</a></td>
            </tr>`;

		        	$tableMain.append(row);
	        	});

	        	$tableExtension.html('');
	        	$.each(data, function(i,item){		
	        		row = 
		        	`<tr>
                        <td>${item.domain}</td>
                        <td>${item.price}</td>
                        <td><a href="#">Agregar +</a></td>
                    </tr>`
                    $tableExtension.append(row);
		        });
	        }
	    });
    }

    if($tablePrefix && keyword){
    	$tablePrefix.html(`<img src="${template_url}/includes/domainer/loading.gif" width="20%" > cargando ...`);
	    jQuery.ajax({
	        type:"POST",
	        url: site_url+"/wp-admin/admin-ajax.php",
	        data: {"action":"searchDomainPrefix","keyword":keyword},
	        success:function(data){
	        	$tablePrefix.html('');
	        	$.each(data, function(i,item){		
	        		row = 
		        	`<tr>
                        <td>${item.domain}</td>
                        <td>${item.price}</td>
                        <td><a href="#">Agregar +</a></td>
                    </tr>`
                    $tablePrefix.append(row);
		        });
	        }
	    });
    }

    if($tableColoquial && keyword){
    	$tableColoquial.html(`<img src="${template_url}/includes/domainer/loading.gif" width="20%" > cargando ...`);
	    jQuery.ajax({
	        type:"POST",
	        url: site_url+"/wp-admin/admin-ajax.php",
	        data: {"action":"searchDomainColoquial","keyword":keyword},
	        success:function(data){
	        	$tableColoquial.html('');
	        	$.each(data, function(i,item){		
	        		row = 
		        	`<tr>
                        <td>${item.domain}</td>
                        <td>${item.price}</td>
                        <td><a href="#">Agregar +</a></td>
                    </tr>`
                    $tableColoquial.append(row);
		        });
	        }
	    });
    }
    }
});