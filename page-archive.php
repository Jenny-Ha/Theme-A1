<?php
/*
Template Name: Archives
*/
get_header(); ?>
    <!--Main-Wrapper-->
    <div class="main-wrapper">
        <!--Barra de título de la página-->
        <header class="header-bar">
            <div class="container">
                <div class="head-page">
                    <h1><span>Blog Hosting A1</span></h1>
                </div>
            </div>
        </header>
        <!--Sección de Blog-->
        <section id="pg-blog" class="main-content container">
            <div class="row">
                <div class="list-posts col-md-9">
                    <div class="posts-here">
                        <?php query_posts('post_type=post&post_status=publish&posts_per_page=10&paged=' . get_query_var('paged')); ?>
                        <?php if (have_posts()) : ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <div class="item-post">
                                    <h3 class="title-entry"><a href="#"><?php the_title(); ?></a></h3>
                                    <div class="post-meta">
                                        <span>
                                            <i aria-hidden="true" class="fa fa-calendar"></i><?php echo get_the_date('F j, Y'); ?>
                                        </span>
                                        <small>/</small>
                                        <span><i aria-hidden="true"
                                                 class="fa fa-comments"></i><?php comments_number(); ?></span>
<!--                                        <small>/</small>-->
<!--                                        <span><i aria-hidden="true" class="fa fa-heart"></i>321</span>-->
                                        <small>/</small>
                                        <span class="category"><i aria-hidden="true" class="fa fa-folder"></i>
                                            <?php
                                            $categories = get_the_category();
                                            foreach ( $categories as $category ) {
                                                printf( '<a href="%1$s">%2$s</a> | ',
                                                    get_category_link($category->id),esc_html( $category->cat_name )
                                                );
                                            }
                                            ?>
                                        </span>
                                    </div>
                                    <div class="post-media"><a
                                            href="#"><?php the_post_thumbnail('single-post-thumbnail'); ?></a></div>
                                    <p class="post-extract"><?php the_excerpt(); ?></p>
                                    <div class="post-more text-right">Compartir
                                        <a href="#" title="Compártelo en Facebook"><i
                                                aria-hidden="true" class="fa fa-facebook"></i></a>
                                        <a href="#" title="Compártelo en Twitter"><i
                                                aria-hidden="true" class="fa fa-twitter"></i></a>
                                        <a href="#" title="Compártelo en Google Plus"><i
                                                aria-hidden="true" class="fa fa-google"></i></a><span>
                                            <a href="<?php the_permalink(); ?>"
                                               class="btn btn-primary">Leer Más</a></span>
                                    </div>
                                </div>
                            <?php endwhile; endif; ?>
                    </div>
                    <div class="post-nav"><span class="nav-prev"><a href="#" class="prev btn-special btn-primary">Anterior</a></span><span
                            class="current nav-number">1</span><a href="#" title="2" class="nav-number">2</a><a href="#"
                                                                                                                title="2"
                                                                                                                class="nav-number">3</a><a
                            href="#" title="2" class="nav-number">4</a><a href="#" title="2" class="nav-number">5</a><a
                            href="#" title="2" class="nav-number">6</a><a href="#" title="2" class="nav-number">7</a><a
                            href="#" title="2" class="nav-number">8 </a><span class="nav-next"><a href="#"
                                                                                                  class="next btn-special btn-primary">Siguiente</a></span>
                    </div>
                </div>
                <!--Sidebar para Blog-->
                <aside id="sidebar" class="col-md-3">
                    <?php dynamic_sidebar('post-sidebar'); ?>
                </aside>
            </div>
        </section>
    </div>

<?php get_footer(); ?>