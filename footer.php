<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="logo-footer col-md-2 col-sm-6">
                <img src="<?php echo get_template_directory_uri()?>/img/logo_a1_gray.png" alt="Logo Hosting A1" class="logo-a1">
            </div>
            <div class="recent-posts col-md-4 col-sm-6">
                <p class="title-1">ENTRADAS RECIENTES
                    <ul class="media-list">
                        <?php 	$recent_posts = wp_get_recent_posts(array('numberposts' => '3'));
                        foreach( $recent_posts as $recent ):
                        ?>

                        <li class="media">
                            <div class="media-left">

                                <a href="<?php echo get_permalink($recent["ID"]) ?>">
                                    <?php echo get_the_post_thumbnail($recent["ID"],'thumbnail', array( 'class' => 'media-object enter' )); ?>
                                </a></div>
                            <div class="media-body">
                                <p class="media-heading"><a href="<?php echo get_permalink($recent["ID"]) ?>"><?php echo $recent["post_title"] ?></a></p>
                                <span class="date">
                                    <i class="fa fa-calendar-o"> </i>Nov 26,2016
                                </span>
                            </div>
                        </li>
            <?php endforeach; wp_reset_query(); ?>
            </ul>
            </p>
        </div>
        <div class="suscription col-md-4 col-sm-6">
            <?php dynamic_sidebar('footer-1-bar'); ?>

        </div>
        <div class="socials col-md-2 col-sm-6">
            <p class="title-1">SÍGUENOS</p>
            <p class="text-center">
                <span><a href="https://www.facebook.com/AQPhost" target="_blank" title="Síguenos en Facebook" class="fa fa-facebook"></a></span>
                <span><a href="#" target="_blank" title="Síguenos en Twitter" class="fa fa-twitter"></a></span>
                <span><a href="#" target="_blank" title="Síguenos en Instagram" class="fa fa-instagram"></a></span>
            </p>
        </div>
    </div>
    <hr class="bar">
        <?php clean_custom_menu("footer-menu","menu-footer clearfix","") ?>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <p class="col-sm-8">Hosting A1. Todos los derechos reservados © 2016 Arequipa - Perú</p>
                <p class="col-sm-4"><span>Powered by</span><a href="http://atixplus.com/" target="_blank" title="Atix Plus" class="logo-atix"></a></p>
            </div>
        </div>
    </div>
    <a id="to-top" href="#top" class="to-top fa fa-chevron-up"></a>
</footer>
<!--Scripts     -->

<?php wp_footer(); ?>
<?php if( strpos(get_permalink(), 'inicio') || is_front_page() ):?>
    <script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/slick.js"></script>
    <!--Script para modificar carousel de bootstrap (sliders)-->
    <script type="text/javascript">
        $(document).ready(function(){
            //Slider de precios en home
            $("#offers-slider").carousel({
                interval: 3500
            });

            //Slider de Banner
            $("#banner-slider").carousel({
                interval: 3500
            });
        })
    </script>
    <!--Script para slider de testimonios en Home-->
    <script type="text/javascript">
        $(document).ready(function(){
            //Función para estilos de Slider para home
            $('.testimonials-slider').slick({
                centerMode      : true,
                centerPadding   : '0px',
                slidesToShow    : 5,
                autoplay        :false,
                autoplaySpeed   :3000,
                arrows          :false,
                dots            :true,
                infinite        :true,
                responsive: [
                    {
                        breakpoint: 1199,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '0px',
                            slidesToShow: 5                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '0px',
                            slidesToShow: 3                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '0px',
                            slidesToShow: 3                        }
                    },
                    {
                        breakpoint: 641,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '0px',
                            slidesToShow: 1                        }
                    }
                ]
            });
        });

        /*Mover slider al hacer clic sobre la imagen*/
        $(".testimonials-slider .image").click(function(){
            var position = $(this).parent().data("slick-index");
            $('.testimonials-slider').slick('slickGoTo', position );
        })

    </script>
    <!--Script para Smooth Scroll-->
    <script type="text/javascript">
        //Función para estilos de smoot scroll
        $(document).ready(function()
        {
            // Ocultar elemento a#to-top
            var windowHeight = window.innerHeight;

            $(window).scroll(function()
            {
                if ($(this).scrollTop() < windowHeight)
                {
                    $('#to-top') .fadeOut();
                } else
                {
                    $('#to-top') .fadeIn();
                }
            });

            // Desplazamiento scrroll hacia arriba de a#to-top
            $('#to-top').on('click', function()
            {
                $('html, body').animate({scrollTop:0}, 'slow');
                return false;
            });

            //Desplazamiento a tablas de caracteristicas
            $("a.to-table").click(function(e) {
                e.preventDefault();
                var link = $(this);
                var anchor  = link.attr('href');
                $("html, body").animate({scrollTop: $(anchor).offset().top}, 'slow');
            });
        });

    </script>
    <!--Scripts para la pagina de Zona-aprendizaje.html -Masonry gallery-->
    <script src="<?php echo get_template_directory_uri()?>/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript">
        var container = document.querySelector('#masonry');
        var masonry = new Masonry(container, {
            itemSelector: '.masonry-item',
            resize      : true
        });

    </script>
    <!--Script para upload file Página de reportar-pago.html-->
    <script>
        $("input#comprobante").change(function()
        {
            $("span#file-archivo").html($(this).val());
        }).css('border-width',function()
        {
            if(navigator.appName == "Microsoft Internet Explorer")
                return 0;
        });

    </script>
    <!--Script para activar los Popover de reportar-pago.html-->
    <script>
        $('[data-toggle="popover"]').popover({
            trigger: 'focus',
            container: 'body'
        });

    </script>
    <!--Script para plugin Datepicker-->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( "#datepicker" ).datepicker({
            maxDate: "+0D",
            dateFormat: "dd-mm-yy"
        });
        $( "#fecha" ).click(function(){
            $( "#datepicker" ).datepicker("show");
        })
    </script>
<?php endif; ?>
</body>
</html>