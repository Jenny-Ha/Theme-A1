<?php get_header(); ?>
<!--Main-Wrapper-->
<div class="main-wrapper">
    <!--Barra de título de la página-->
    <header class="header-bar">
        <div class="container">
            <div class="head-page">
                <h1><span>Blog Hosting A1</span></h1>
            </div>
        </div>
    </header>
    <!--Sección de Blog-->
    <section id="pg-blog" class="main-content container">
        <div class="row">

            <div class="post-entry col-md-9">
                <?php
                // Start the loop.
                while (have_posts()) : the_post(); ?>

                    <?php get_template_part('content');

                    if (comments_open() || get_comments_number()) :
                        comments_template();
                    endif;

                endwhile;
                ?>

                <div class="similar-posts widget">
                    <h3 class="widget-title">Podría interesarte</h3>
                    <?php $prev_post = get_previous_post();
                    if (!empty($prev_post)):
                        ?>
                        <div class="pull-left">
                            <div class="item">
                                <a href="<?php echo get_permalink($prev_post->ID); ?>" class="media-content">
                                    <?php echo get_the_post_thumbnail($prev_post->ID, 'thumbnail', array('class' => 'media-object enter')); ?></a>
                                <a href="<?php echo get_permalink($prev_post->ID); ?>"><h5
                                        class="widget-title"><?php echo $prev_post->post_title; ?></h5></a>
                                <div class="post-meta"><span><i aria-hidden="true"
                                                                class="fa fa-calendar"></i><?php echo get_the_date('F j, Y', $prev_post->ID); ?></span>
                                    <small>/</small>
                                    <span><i aria-hidden="true" class="fa fa-comments"></i>123</span>
                                    <small>/</small>
                                    <span><i aria-hidden="true" class="fa fa-heart"></i>321</span></div>
                            </div>
                        </div>
                        <?php
                    endif;
                    $next_post = get_next_post();
                    if (!empty($next_post)):
                        ?>
                        <div class="pull-left">
                            <div class="item">
                                <a href="<?php echo get_permalink($next_post->ID); ?>" class="media-content">
                                    <?php echo get_the_post_thumbnail($next_post->ID, 'thumbnail', array('class' => 'media-object enter')); ?>
                                </a>
                                <a href="<?php echo get_permalink($next_post->ID); ?>"><h5
                                        class="widget-title"><?php echo $next_post->post_title; ?></h5></a>
                                <div class="post-meta">
                                <span>
                                    <i aria-hidden="true" class="fa fa-calendar"></i>
                                    <?php echo get_the_date('F j, Y', $next_post->ID); ?></span>
                                    <small>/</small>
                                    <span><i aria-hidden="true" class="fa fa-comments"></i>123</span>
                                    <small>/</small>
                                    <span><i aria-hidden="true" class="fa fa-heart"></i>321</span>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
                <!--Lista de Comentarios         -->
                <div class="post-comments">
                    <h3 class="widget-title">Comentarios</h3>
                    <ul class="comment-list">
                        <li>
                            <div class="comment">
                                <h5>WALTHER AYALA<span class="time">Ago. 08, 2016</span></h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate.</p>
                                <div class="meta-data">
                                    <div class="value"><span><a class="fa fa-thumbs-up"> </a>12</span><span><a
                                                class="fa fa-comment"> </a>5</span></div>
                                    <a href="#" class="reply">Responder</a>
                                </div>
                                <!--Respuesta a comentario-->
                            </div>
                            <ul class="comments-reply">
                                <li>
                                    <div class="comment">
                                        <h5>WALTHER AYALA<span class="time">Ago. 08, 2016</span></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod
                                            bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra
                                            justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus
                                            et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum,
                                            nulla luctus pharetra vulputate.</p>
                                        <div class="meta-data">
                                            <div class="value"><span><a class="fa fa-thumbs-up"> </a>12</span><span><a
                                                        class="fa fa-comment"> </a>5</span></div>
                                            <a href="#" class="reply">Responder</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <div class="comment">
                                <h5>WALTHER AYALA<span class="time">Ago. 08, 2016</span></h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                                    laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
                                    sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient
                                    montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate.</p>
                                <div class="meta-data">
                                    <div class="value"><span><a class="fa fa-thumbs-up"> </a>12</span><span><a
                                                class="fa fa-comment"> </a>5</span></div>
                                    <a href="#" class="reply">Responder</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="more-comments"><a href="#" class="btn-special btn-primary">Cargar más comentarios</a>
                    </div>
                </div>
                <div class="leave-comment">
                    <h3 class="widget-title">ENVÍANOS TUS COMENTARIOS</h3>
                    <form class="comment-form">
                        <p class="form-inline clearfix">
                            <input type="text" placeholder="Nombre" class="form-control">
                            <input type="text" placeholder="Apellidos" class="form-control">
                        </p>
                        <p>
                            <input type="email" placeholder="Correo electrónico" class="form-control">
                        </p>
                        <p>
                            <textarea placeholder="Deja aquí tu comentario" class="form-control"></textarea>
                        </p>
                        <p class="text-right">
                            <input type="submit" value="Enviar comentario" class="btn btn-default">
                        </p>
                    </form>
                </div>
            </div>


            <!--Sidebar para Blog-->
            <aside id="sidebar" class="col-md-3">
                <?php dynamic_sidebar('post-sidebar'); ?>
            </aside>
        </div>
    </section>
</div>
<?php get_footer(); ?>
