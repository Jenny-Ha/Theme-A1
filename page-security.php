<?php
/*
Template Name: Security
*/
get_header(); ?>

    <section id="pg-ssl" class="main-content">
        <!--Sección Hero-->
        <div class="ssl-hero hero">
            <div class="container">
                <div class="wrapper-content">
                    <div class="hero-content text-center">
                        <h2 class="text-primary">QUE ESPERAS PARA ADQUIRIR</h2>
                        <H1>EL SSL MÁS ADECUADO Y SENCILLO</H1>
                        <div class="price-plan btn-group">
                            <button type="button" class="btn btn-input"><span class="text-primary">1 año </span>-
                                S/.<span class="text-primary">14.00 </span>al mes
                            </button>
                            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                    class="btn btn-primary dropdown-toggle"><span class="caret"></span><span
                                    class="sr-only">Toggle Dropdown</span></button>
                            <ul class="list-price dropdown-menu">
                                <li><a href="#"><span class="text-primary">1 año </span>- S/.<span class="text-primary">14.00 </span>al
                                        mes</a></li>
                                <li><a href="#"><span class="text-primary">2 años </span>- S/.<span
                                            class="text-primary">12.00 </span>al mes</a></li>
                            </ul>
                            <span class="igv">IGV incluido*</span>
                        </div>
                        <a href="#" class="btn btn-primary"><i class="fa fa-shopping-cart"> </i> ADQUIRIR</a>
                        <div class="info"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error illum
                            exercitationem neque eum non id nesciunt, facere aperiam molestiae ea alias nulla,
                            dignissimos aliquam sequi dolorem aspernatur necessitatibus modi. Iusto.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Sección de Slider-->
        <div class="ssl-banner banner">
            <div class="container">
                <!--Inicio de slider de banner-->
                <div id="banner-slider" data-ride="carousel" class="carousel slide">
                    <!-- Indicators-->
                    <ol class="carousel-indicators sr-only">
                        <li data-target="#banner-slider" data-slide-to="0" class="active"></li>
                        <li data-target="#banner-slider" data-slide-to="1"></li>
                    </ol>
                    <!-- Wrapper for slides-->
                    <div role="listbox" class="carousel-inner">
                        <div class="item active">
                            <div class="banner-content clearfix">
                                <div class="info-banner col-md-4 col-sm-6">
                                    <p class="text-primary"> EN SEPTIEMBRE</p>
                                    <h2>HOSTING WORDPRESS </h2>
                                    <p class="text-primary"><span class="f-20">50% </span>de DESCUENTO</p>
                                </div>
                                <div class="info-banner col-md-4 col-sm-6">
                                    <p>CODIGO: AYUDA</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac mollis
                                        ante, eu facilisis metus. Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit. </p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="banner-content clearfix">
                                <div class="info-banner col-md-4 col-sm-6">
                                    <p class="text-primary"> EN SEPTIEMBRE</p>
                                    <h2>HOSTING WORDPRESS </h2>
                                    <p class="text-primary"><span class="f-20">50% </span>de DESCUENTO</p>
                                </div>
                                <div class="info-banner col-md-4 col-sm-6">
                                    <p>CODIGO: AYUDA</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac mollis
                                        ante, eu facilisis metus. Lorem ipsum dolor sit amet, consectetur adipiscing
                                        elit. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Left and right controls--><a href="#banner-slider" role="button" data-slide="prev"
                                                      class="left carousel-control"><i aria-hidden="true"
                                                                                       class="icon-prev"></i><span
                            class="sr-only">Previous</span></a><a href="#banner-slider" role="button" data-slide="next"
                                                                  class="right carousel-control"><i aria-hidden="true"
                                                                                                    class="icon-next"></i><span
                            class="sr-only">Next</span></a>
                </div>
                <!--Fin de slider-->
            </div>
        </div>
        <!--Sección Plan SSL-->
        <div class="section-ssl-plans price-plans">
            <div class="container">
                <h2>TODOS NUESTROS PLANES</h2>
                <table class="table plan-precios">
                    <tbody>
                    <tr>
                        <td class="ssl-logo bg-gray-02"><img src="../img/security_logo.png"></td>
                        <td class="ssl-info bg-gray-02">
                            <h5>SSL 123 Certificates</h5>
                            <p>Protege un solo dominio, con y sin www<br>Ej: tusitio.com y www.tusitio.com</p>
                        </td>
                        <td class="price"><span class="moneda">S/.</span>999.00
                            <small>X AÑO</small>
                        </td>
                        <td class="buy"><a href="#" class="btn btn-primary">COMPRAR</a></td>
                    </tr>
                    <tr>
                        <td class="ssl-logo bg-gray-02"><img src="../img/security_logo.png"></td>
                        <td class="ssl-info bg-gray-02">
                            <h5>SSL 123 Certificates</h5>
                            <p>Protege un solo dominio, con y sin www<br>Ej: tusitio.com y www.tusitio.com</p>
                        </td>
                        <td class="price"><span class="moneda">S/.</span>999.00
                            <small>X AÑO</small>
                        </td>
                        <td class="buy"><a href="#" class="btn btn-primary">COMPRAR</a></td>
                    </tr>
                    </tbody>
                </table>
                <p class="text-left"><a href="#ssl-features" class="to-features">* Ver todas las caracteristicas</a></p>
                <h5 class="subtitle">¿QUIERES SABER CUAL ES EL PLAN QUE SE ADAPTA A TUS NECESIDADES?</h5><a href="#"
                                                                                                            class="btn btn-primary">
                    <i class="fa fa-comment"> </i>HABLAR CON UN EXPERTO</a>
            </div>
        </div>
        <div class="ssl-benefits section-benefits">
            <div class="container">
                <h2>Nuestros Beneficios</h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates enim iste provident vitae
                    exercitationem assumenda aliquid ad, culpa sint doloremque voluptatem animi totam rerum velit,
                    maiores non. Unde, amet illum?</p>
                <div class="row">
                    <div class="media col-md-4">
                        <div class="media-left"><span><img src="../img/icon_caracteristica.png"
                                                           alt="Soporte 24/7"/></span></div>
                        <div class="media-body">
                            <h5>Soporte 24/7</h5>
                            <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem dantium,
                                totam rem aperiam, eaque ipsa.</p>
                        </div>
                    </div>
                    <div class="media col-md-4">
                        <div class="media-left"><span><img src="../img/icon_caracteristica.png"
                                                           alt="Soporte 24/7"/></span></div>
                        <div class="media-body">
                            <h5>Soporte 24/7</h5>
                            <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem dantium,
                                totam rem aperiam, eaque ipsa.</p>
                        </div>
                    </div>
                    <div class="media col-md-4">
                        <div class="media-left"><span><img src="../img/icon_caracteristica.png"
                                                           alt="Soporte 24/7"/></span></div>
                        <div class="media-body">
                            <h5>Soporte 24/7</h5>
                            <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem dantium,
                                totam rem aperiam, eaque ipsa.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="media col-md-4">
                        <div class="media-left"><span><img src="../img/icon_caracteristica.png"
                                                           alt="Soporte 24/7"/></span></div>
                        <div class="media-body">
                            <h5>Soporte 24/7</h5>
                            <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem dantium,
                                totam rem aperiam, eaque ipsa.</p>
                        </div>
                    </div>
                    <div class="media col-md-4">
                        <div class="media-left"><span><img src="../img/icon_caracteristica.png"
                                                           alt="Soporte 24/7"/></span></div>
                        <div class="media-body">
                            <h5>Soporte 24/7</h5>
                            <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem dantium,
                                totam rem aperiam, eaque ipsa.</p>
                        </div>
                    </div>
                    <div class="media col-md-4">
                        <div class="media-left"><span><img src="../img/icon_caracteristica.png"
                                                           alt="Soporte 24/7"/></span></div>
                        <div class="media-body">
                            <h5>Soporte 24/7</h5>
                            <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem dantium,
                                totam rem aperiam, eaque ipsa.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Sección  de Ofertas -->
        <div class="ssl-offers offers-content">
            <div class="container">
                <div class="media">
                    <div class="media-body media-middle">
                        <h2>ADEMAS TE AFRECEMOS</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                            laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                            architecto beatae vitae dicta sunt explicabo.</p>
                    </div>
                    <div class="media-right"><span><img src="../img/hosting_img.jpg" alt="ADEMAS TE AFRECEMOS"/></span>
                    </div>
                </div>
                <div class="media">
                    <div class="media-left"><span><img src="../img/hosting_img.jpg" alt="Y ESO NO ES TODO"/></span>
                    </div>
                    <div class="media-body media-middle">
                        <h2>Y ESO NO ES TODO</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                            laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                            architecto beatae vitae dicta sunt explicabo. </p>
                    </div>
                </div>
            </div>
        </div>
        <!--Sección FAQ-->
        <div class="ssl-FAQ section-faq">
            <div class="container">
                <h2>PREGUNTAS FRECUENTES
                </h2>
                <div class="item-faq">
                    <div id="#heading-1" role="tab">
                        <h5><a href="#collapse-1" role="button" data-toggle="collapse" data-parent="#accordion"
                               aria-expanded="true" aria-controls="collapse-1">¿Qué es un servidor VPS?</a></h5>
                    </div>
                    <div id="collapse-1" role="tabpanel" aria-labelledby="heading-1" aria-expanded="true"
                         class="collapse in">
                        <div class="panel-body">
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi beatae molestias esse,
                                aliquam similique architecto at omnis vel, neque, suscipit vero totam eveniet. Illum,
                                distinctio atque modi autem at doloremque!</p>
                            <h5 class="subtitle">SUBTITULO</h5>
                            <ul class="list-circle">
                                <li>Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</li>
                                <li>Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</li>
                                <li>Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</li>
                            </ul>
                            <h5 class="subtitle">SUBTITULO</h5>
                            <ol class="list-numbered">
                                <li>lorem ipsum</li>
                                <li>lorem ipsum</li>
                                <li>lorem ipsum</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="item-faq">
                    <div id="#heading-2" role="tab">
                        <h5><a href="#collapse-2" role="button" data-toggle="collapse" data-parent="#accordion"
                               aria-expanded="false" aria-controls="collapse-2" class="collapsed">¿Cómo registro mi
                                servidor VPS?</a></h5>
                    </div>
                    <div id="collapse-2" role="tabpanel" aria-labelledby="heading-2" aria-expanded="false"
                         class="collapse">
                        <div class="panel-body">
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo aliquam inventore
                                ex, placeat obcaecati dolorem blanditiis harum quam qui officia consequuntur quae
                                laborum, doloremque quaerat nisi impedit exercitationem illum expedita!
                            <ol class="list-numbered">
                                <li>lorem ipsum</li>
                                <li>lorem ipsum</li>
                                <li>lorem ipsum</li>
                            </ol>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="item-faq">
                    <div id="#heading-3" role="tab">
                        <h5><a href="#collapse-3" role="button" data-toggle="collapse" data-parent="#accordion"
                               aria-expanded="false" aria-controls="collapse-3" class="collapsed">¿En qué formas puedo
                                pagar?</a></h5>
                    </div>
                    <div id="collapse-3" role="tabpanel" aria-labelledby="heading-3" aria-expanded="false"
                         class="collapse">
                        <div class="panel-body">
                            <p>No se ha agregado ningun contenido</p>
                        </div>
                    </div>
                </div>
                <div class="item-faq">
                    <div id="#heading-4" role="tab">
                        <h5><a href="#collapse-4" role="button" data-toggle="collapse" data-parent="#accordion"
                               aria-expanded="false" aria-controls="collapse-4" class="collapsed">¿Es factible cambiarme
                                a HOSTING A1 para que sea mi Registrante?</a></h5>
                    </div>
                    <div id="collapse-4" role="tabpanel" aria-labelledby="heading-4" aria-expanded="false"
                         class="collapse">
                        <div class="panel-body">
                            <p>No se ha agregado ningun contenido</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Sección de Características de Planes-->
        <div id="ssl-features" class="table-features">
            <div class="container">
                <h2>TODAS LAS CARACTERISTICAS DE NUESTROS PLANES</h2>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th>BÁSICO</th>
                        <th>AVANZADO</th>
                        <th>PROFESIONAL</th>
                        <th>PREMIUM</th>
                        <th>CORPORATIV0</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Espacio de Almacenamiento</td>
                        <td>100 MB</td>
                        <td>200 MB</td>
                        <td>100 MB</td>
                        <td>SI</td>
                        <td>NO</td>
                    </tr>
                    <tr>
                        <td>Transferencia Mensual</td>
                        <td>Ilimitado</td>
                        <td>50 GB</td>
                        <td>1 TB</td>
                        <td>100TB</td>
                        <td>SI</td>
                    </tr>
                    <tr>
                        <td>Espacio de Almacenamiento</td>
                        <td>100 MB</td>
                        <td>200 MB</td>
                        <td>100 MB</td>
                        <td>SI</td>
                        <td>NO</td>
                    </tr>
                    <tr>
                        <td>Transferencia Mensual</td>
                        <td>Ilimitado</td>
                        <td>50 GB</td>
                        <td>1 TB</td>
                        <td>100TB</td>
                        <td>SI</td>
                    </tr>
                    <tr>
                        <td>Espacio de Almacenamiento</td>
                        <td>100 MB</td>
                        <td>200 MB</td>
                        <td>100 MB</td>
                        <td>SI</td>
                        <td>NO</td>
                    </tr>
                    <tr>
                        <td>Transferencia Mensual</td>
                        <td>Ilimitado</td>
                        <td>50 GB</td>
                        <td>1 TB</td>
                        <td>100TB</td>
                        <td>SI</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td>
                        <td><a href="#">Adquirir</a></td>
                        <td><a href="#">Adquirir </a></td>
                        <td><a href="#">Adquirir</a></td>
                        <td><a href="#">Adquirir</a></td>
                        <td><a href="#">Adquirir</a></td>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="section-learning">
            <div class="container">
                <h2>HOSTING A1 TE ENSEÑA</h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, perferendis, amet! Numquam tempore
                    deleniti a amet quod minima iure veritatis aliquam expedita. Commodi odit repellendus, officiis est
                    aliquam perferendis. Pariatur.</p><a href="#" class="btn btn-primary">Acceder al centro de
                    aprendizaje</a>
            </div>
        </div>
        <div class="section-way-pay">
            <div class="container"><a href="#" class="btn btn-primary"><i class="fa fa-ticket"> </i>Reporte su pago aquí</a>O
                elija otras formas de pago<span><a href="#"><img src="../img/icon_pago1.jpg" class="payment"></a><a
                        href="#"><img src="../img/icon_pago2.jpg" class="payment"></a><a href="#"><img
                            src="../img/icon_pago3.jpg" class="payment"></a><a href="#"><img src="../img/icon_pago4.jpg"
                                                                                             class="payment"></a><a
                        href="#"><img src="../img/icon_pago1.jpg" class="payment"></a><a href="#"><img
                            src="../img/icon_pago2.jpg" class="payment"></a><a href="#"><img src="../img/icon_pago3.jpg"
                                                                                             class="payment"></a><a
                        href="#"><img src="../img/icon_pago4.jpg" class="payment"></a><a href="#"><img
                            src="../img/icon_pago1.jpg" class="payment"></a></span></div>
        </div>
    </section>
<?php get_footer(); ?>