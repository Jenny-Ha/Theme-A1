<?php get_header(); ?>
<div class="main-wrapper">
    <section id="pg-home" class="main-content">
        <?php while (have_posts()) : the_post();
            a1_the_content();
            ?>
        <?php endwhile; ?>
    </section>
</div>
<?php get_footer(); ?>
