<form action="/" method="get">
    <div class="input-group">
        <input type="text" placeholder="Buscar..." class="form-control" value="<?php the_search_query(); ?>">
        <span class="input-group-btn">
        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
    </div>
</form>