<?php
/*
Template Name: Menu Left
*/
get_header(); ?>
<div class="main-wrapper">
	<?php while (have_posts()) : the_post(); ?>
	  <!--Barra de título de la página-->
	<header class="header-bar">
        <div class="container">
	      <div class="head-page">
	        <h1><span><?php the_title();?></span></h1>
	      </div>
      </div>
   </header>
      <!--Sección principal de Nosotros-->
      <section id="pg-nosotros" class="main-content container">
        <div class="row">
          <div class="nav-nosotros col-md-3">
            <div class="nav-vertical">
            <?php clean_custom_menu("left_menu","nav nav-stacked","") ?>
              <!-- <ul class="nav nav-stacked">
                <li><a href="nosotros.html">¿Quienes somos?</a></li>
                <li><a href="nuestra-organizacion.html">Nuestra Organización</a></li>
                <li><a href="soporte.html">Soporte</a></li>
                <li><a href="terminos-y-condiciones.html">Terminos y condiciones</a></li>
                <li><a href="politica-de-privacidad.html">Política de Privacidad</a></li>
                <li><a href="terminos-de-registro-de-dominios.html">Terminos de registro de dominio    </a></li>
              </ul> -->
            </div>
          </div>
          <div class="main col-md-9">
			<?php a1_the_content(); ?>            
          </div>
        </div>
      </section>
    </div>

    <?php endwhile; ?>



<?php get_footer(); ?>

