<?php
/*
Template Name: Domains Transfer
*/
get_header(); ?>

    <div class="main-wrapper">
        <!--Sección principal de Dominios-->
        <section id="pg-dominios-transfer" class="main-content">
            <!--Sección Hero-->
            <div class="hero-domain-transfer hero">
                <div class="container">
                    <div class="wrapper-content">
                        <div class="hero-content text-center">
                            <h2 class="text-primary">AHORA PUEDES</h2>
                            <h1>BUSCAR TU DOMINIO A TRANSFERIR</h1>
                            <form class="search-domain form-inline" action=".">
                                <div class="form-group">
                                    <input type="text" name="keyword" value="Busca tu dominio" class="form-control">
                                    <input type="submit" value="Buscar Dominio" class="form-control btn btn-primary">
                                </div>
                                >>>  <?php echo $_GET['keyword']; ?>  <<<
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--Sección Beneficios-->
            <div class="section-benefits bg-white">
                <div class="container">
                    <h2>Nuestros Beneficios</h2>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates enim iste provident vitae
                        exercitationem assumenda aliquid ad, culpa sint doloremque voluptatem animi totam rerum velit,
                        maiores non. Unde, amet illum?</p>
                    <div class="row">
                        <div class="media col-md-4">
                            <div class="media-left"><span><img src="<?php echo get_template_directory_uri()?>/img/icon_caracteristica.png" alt="Soporte 24/7"/></span>
                            </div>
                            <div class="media-body">
                                <h5>Soporte 24/7</h5>
                                <p>No Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus atque
                                    cupiditate, numquam laboriosam maxime, consequatur voluptates aspernatur modi
                                    repellendus. Ad facilis consequuntur sunt repellat ut cupiditate ex molestiae
                                    expedita tenetur.</p>
                            </div>
                        </div>
                        <div class="media col-md-4">
                            <div class="media-left"><span><img src="<?php echo get_template_directory_uri()?>/img/icon_caracteristica.png" alt="Soporte 24/7"/></span>
                            </div>
                            <div class="media-body">
                                <h5>Soporte 24/7</h5>
                                <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                    dantium, totam rem aperiam, eaque ipsa.</p>
                            </div>
                        </div>
                        <div class="media col-md-4">
                            <div class="media-left"><span><img src="<?php echo get_template_directory_uri()?>/img/icon_caracteristica.png" alt="Soporte 24/7"/></span>
                            </div>
                            <div class="media-body">
                                <h5>Soporte 24/7</h5>
                                <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                    dantium, totam rem aperiam, eaque ipsa.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="media col-md-4">
                            <div class="media-left"><span><img src="<?php echo get_template_directory_uri()?>/img/icon_caracteristica.png" alt="Soporte 24/7"/></span>
                            </div>
                            <div class="media-body">
                                <h5>Soporte 24/7</h5>
                                <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                    dantium, totam rem aperiam, eaque ipsa.</p>
                            </div>
                        </div>
                        <div class="media col-md-4">
                            <div class="media-left"><span><img src="<?php echo get_template_directory_uri()?>/img/icon_caracteristica.png" alt="Soporte 24/7"/></span>
                            </div>
                            <div class="media-body">
                                <h5>Soporte 24/7</h5>
                                <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                    dantium, totam rem aperiam, eaque ipsa.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Preguntas más frecuentes-->
            <div class="section-faq">
                <div class="container">
                    <h2>PREGUNTAS FRECUENTES</h2>
                    <div class="item-faq">
                        <div id="#heading-1" role="tab">
                            <h5><a href="#collapse-1" role="button" data-toggle="collapse" data-parent="#accordion"
                                   aria-expanded="true" aria-controls="collapse-1">¿Qué es un dominio?</a></h5>
                        </div>
                        <div id="collapse-1" role="tabpanel" aria-labelledby="heading-1" aria-expanded="true"
                             class="collapse in">
                            <div class="panel-body">
                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe tempore voluptatem
                                    eveniet facilis nesciunt et odit, in nostrum ipsa a recusandae doloribus ea amet
                                    delectus, velit itaque pariatur sapiente natus.</p>
                                <h5 class="subtitle">SUBTITULO</h5>
                                <ul class="list-circle">
                                    <li>Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</li>
                                    <li>Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</li>
                                    <li>Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</li>
                                </ul>
                                <h5 class="subtitle">SUBTITULO</h5>
                                <ol class="list-numbered">
                                    <li>lorem ipsum</li>
                                    <li>lorem ipsum</li>
                                    <li>lorem ipsum</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="item-faq">
                        <div id="#heading-2" role="tab">
                            <h5><a href="#collapse-2" role="button" data-toggle="collapse" data-parent="#accordion"
                                   aria-expanded="false" aria-controls="collapse-2" class="collapsed">¿Cómo registro mi
                                    dominio?</a></h5>
                        </div>
                        <div id="collapse-2" role="tabpanel" aria-labelledby="heading-2" aria-expanded="false"
                             class="collapse">
                            <div class="panel-body">
                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo aliquam
                                    inventore ex, placeat obcaecati dolorem blanditiis harum quam qui officia
                                    consequuntur quae laborum, doloremque quaerat nisi impedit exercitationem illum
                                    expedita!</p>
                                <ol class="list-numbered">
                                    <li>lorem ipsum</li>
                                    <li>lorem ipsum</li>
                                    <li>lorem ipsum</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="item-faq">
                        <div id="#heading-3" role="tab">
                            <h5><a href="#collapse-3" role="button" data-toggle="collapse" data-parent="#accordion"
                                   aria-expanded="false" aria-controls="collapse-3" class="collapsed">¿En qué formas
                                    puedo pagar mi dominio?</a></h5>
                        </div>
                        <div id="collapse-3" role="tabpanel" aria-labelledby="heading-3" aria-expanded="false"
                             class="collapse">
                            <div class="panel-body">
                                <PAGOS>EN LÍNEA: Ud. puede pagar en línea con su Tarjeta de Crédito, mediante su cuenta
                                    Paypal y con su cuenta Moneybookers lo que le permitirá una activación inmediata.
                                </PAGOS>
                            </div>
                        </div>
                    </div>
                    <div class="item-faq">
                        <div id="#heading-4" role="tab">
                            <h5><a href="#collapse-4" role="button" data-toggle="collapse" data-parent="#accordion"
                                   aria-expanded="false" aria-controls="collapse-4" class="collapsed">Tengo registrado
                                    mi dominio con otro proveedor. ¿Es factible cambiarme a HOSTING A1 para que sea mi
                                    Registrante?</a></h5>
                        </div>
                        <div id="collapse-4" role="tabpanel" aria-labelledby="heading-4" aria-expanded="false"
                             class="collapse">
                            <div class="panel-body">
                                <ol class="list-numbered">
                                    <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                                    <li>In nobis ratione eaque autem amet, pariatur obcaecati distinctio vitae sit
                                        labore voluptate aperiam enim voluptatibus
                                    </li>
                                    <li>It dolorum magni deleniti tempora aliquid, recusandae!</li>
                                </ol>
                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. A qui, dolore, neque
                                    placeat ipsa molestias quia aliquid, quibusdam reprehenderit consectetur odit
                                    recusandae repellendus, possimus quos unde. Quis, sint architecto perspiciatis?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-best-of-us">
                <div class="container">
                    <h2>LO MEJOR DE NOSOTROS A TU SERVICIO</h2>
                    <div class="row">
                        <div class="col-md-3 col-sm-6"><img src="<?php echo get_template_directory_uri()?>/img/icon_nos_1.png">
                            <h5>LOS MEJORES<br>PRECIOS EN OFERTA</h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus
                                eaque ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                        </div>
                        <div class="col-md-3 col-sm-6"><img src="<?php echo get_template_directory_uri()?>/img/icon_nos_2.png">
                            <h5>SOPORTE <br>PROFESIONAL</h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus
                                eaque ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                        </div>
                        <div class="col-md-3 col-sm-6"><img src="<?php echo get_template_directory_uri()?>/img/icon_nos_3.png">
                            <h5>GARANTÍA<br>DE SATISFACCIÓN</h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus
                                eaque ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                        </div>
                        <div class="col-md-3 col-sm-6"><img src="<?php echo get_template_directory_uri()?>/img/icon_nos_4.png">
                            <h5>SEGURIDAD <br>ESPECIALIZADA</h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus
                                eaque ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-learning">
                <div class="container">
                    <h2>HOSTING A1 TE ENSEÑA</h2>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, perferendis, amet! Numquam
                        tempore deleniti a amet quod minima iure veritatis aliquam expedita. Commodi odit repellendus,
                        officiis est aliquam perferendis. Pariatur.</p><a href="#" class="btn btn-primary">Acceder al
                        centro de aprendizaje</a>
                </div>
            </div>
            <div class="section-way-pay">
                <div class="container"><a href="#" class="btn btn-primary"><i class="fa fa-ticket"> </i>Reporte su pago
                        aquí</a>O elija otras formas de pago<span><a href="#"><img src="<?php echo get_template_directory_uri()?>/img/icon_pago1.jpg"
                                                                                   class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri()?>/img/icon_pago2.jpg" class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri()?>/img/icon_pago3.jpg" class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri()?>/img/icon_pago4.jpg" class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri()?>/img/icon_pago1.jpg" class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri()?>/img/icon_pago2.jpg" class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri()?>/img/icon_pago3.jpg" class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri()?>/img/icon_pago4.jpg" class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri()?>/img/icon_pago1.jpg" class="payment"></a></span></div>
            </div>
        </section>
    </div>

<?php get_footer(); ?>