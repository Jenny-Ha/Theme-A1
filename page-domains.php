<?php
/*
Template Name: Domains
*/
get_header(); ?>
<?php $keyword = $_GET['keyword']; ?>
    <div class="main-wrapper">
        <!--Sección principal de Dominios-->
        <section id="pg-dominios" class="main-content">
            <?php if (!isset($keyword)): ?>
                <!--Sección Hero-->
                <div class="hero-domain hero">
                    <div class="container">
                        <div class="wrapper-content">
                            <div class="hero-content text-center">
                                <h2 class="text-primary">AHORA PUEDES DESCUBRIR</h2>
                                <H1>EL DOMINIO PERFECTO</H1>
                                <form class="search-domain form-inline">
                                    <div class="form-group">
                                        <input type="text" name="keyword" placeholder="BUSCA EL NOMBRE DE TU DOMINIO"
                                               class="form-control">
                                        <input type="submit" value="Buscar Dominio"
                                               class="form-control btn btn-primary">
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="price-offer col-lg-2 col-lg-offset-2 col-sm-4"><span
                                            class="text-primary">.com </span>consíguelo
                                        a sólo
                                        <p class="price text-primary">S/. <span>35.00</span><span
                                                class="igv">IGV incluido*</span></p>
                                    </div>
                                    <div class="price-offer col-lg-2 col-lg-offset-1 col-sm-4"><span
                                            class="text-primary">.net  </span>consíguelo
                                        a sólo
                                        <p class="price text-primary">S/. <span>90.00</span><span
                                                class="igv">IGV incluido*</span></p>
                                    </div>
                                    <div class="price-offer col-lg-2 col-lg-offset-1 col-sm-4"><span
                                            class="text-primary">.biz </span>consíguelo
                                        a sólo
                                        <p class="price text-primary">S/. <span>85.00</span><span
                                                class="igv">IGV incluido*</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Sección Banner-->
                <div id="banner-domains">
                    <div class="banner container">
                        <div class="banner-content row">
                            <div class="info-banner col-md-4 col-sm-6">
                                <p class="text-primary"> EN SEPTIEMBRE</p>
                                <h2>HOSTING WORDPRESS </h2>
                                <p class="text-primary"><span class="f-20">50% </span>de DESCUENTO</p>
                            </div>
                            <div class="info-banner col-md-4 col-sm-6">
                                <p>CODIGO: AYUDA</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac mollis ante,
                                    eu
                                    facilisis metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Sección Promociones de Dominios-->
                <div class="promotion-domains container">
                    <h2 class="text-center"> TODOS LOS DOMINIOS QUE QUIERAS</h2>
                    <div class="row">
                        <div class="card-wrap col-md-3 col-xs-6">
                            <div class="price-card">
                                <div class="extension">.pe</div>
                                <div class="description">
                                    <p>¿Que estas esperando?<br/>Consíguelo ahora a sólo</p>
                                    <p class="text-primary"> S/. <span>38.00</span></p><a href="#" target="_blank"
                                                                                          class="btn btn-primary">Comprar
                                        Ahora </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-wrap col-md-3 col-xs-6">
                            <div class="price-card">
                                <div class="extension">.club</div>
                                <div class="description">
                                    <p>¿Que estas esperando?<br/>Consíguelo ahora a sólo</p>
                                    <p class="text-primary"> S/. <span>38.00</span></p><a href="#" target="_blank"
                                                                                          class="btn btn-primary">Comprar
                                        Ahora </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-wrap col-md-3 col-xs-6">
                            <div class="price-card">
                                <div class="extension">.com</div>
                                <div class="description">
                                    <p>¿Que estas esperando?<br/>Consíguelo ahora a sólo</p>
                                    <p class="text-primary"> S/. <span>38.00</span></p><a href="#" target="_blank"
                                                                                          class="btn btn-primary">Comprar
                                        Ahora </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-wrap col-md-3 col-xs-6">
                            <div class="price-card">
                                <div class="extension">.mx</div>
                                <div class="description">
                                    <p>¿Que estas esperando?<br/>Consíguelo ahora a sólo</p>
                                    <p class="text-primary"> S/. <span>38.00</span></p><a href="#" target="_blank"
                                                                                          class="btn btn-primary">Comprar
                                        Ahora </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <?php $result = search_domain($keyword, 'com'); ?>
                <!--Sección Hero-->
                <div class="hero-domain-search hero">
                    <div class="container">
                        <div class="wrapper-content">
                            <div class="hero-content text-center">
                                <?php if ($result): ?>
                                    <i class="fa fa-check available"></i>
                                    <h5 class="available text-primary">DOMINIO DISPONIBLE</h5>
                                    <h1><?php echo $keyword; ?>
                                        <span class="text-primary">S/. 135.00 </span><span
                                            class="igv">IGV incluido*</span>
                                    </h1>
                                    <a href="#" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>comprar</a>
                                <?php else: ?>
                                    <i class="fa fa-close not-available"></i>
                                    <h5 class="not-available text-primary">DOMINIO NO DISPONIBLE</h5>
                                    <h1><?php echo $keyword; ?></h1>
                                <?php endif; ?>
                                <form class="search-domain form-inline" action=".">
                                    <div class="form-group">
                                        <input type="text" name="keyword" value="<?php echo $keyword; ?>"
                                               class="form-control">
                                        <input type="submit" value="Buscar Dominio"
                                               class="form-control btn btn-primary">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Sección Promociones de Dominios-->
                <div class="promotion-domains container">
                    <h2 class="text-center"> TODOS LOS DOMINIOS QUE QUIERAS</h2>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="card-wrap col-xs-6">
                                    <div class="price-card">
                                        <div class="extension">.club</div>
                                        <div class="description">
                                            <p>¿Que estas esperando?<br/>Consíguelo ahora a sólo</p>
                                            <p class="text-primary"> S/. <span>180.00</span></p><a href="#"
                                                                                                   target="_blank"
                                                                                                   class="btn btn-primary">Comprar
                                                Ahora </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-wrap col-xs-6">
                                    <div class="price-card">
                                        <div class="extension">.pe</div>
                                        <div class="description">
                                            <p>¿Que estas esperando?<br/>Consíguelo ahora a sólo</p>
                                            <p class="text-primary"> S/. <span>38.00</span></p><a href="#"
                                                                                                  target="_blank"
                                                                                                  class="btn btn-primary">Comprar
                                                Ahora </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="card-wrap-s col-xs-4">
                                    <div class="price-card small">
                                        <div class="extension">.mx</div>
                                        <div class="description">
                                            <p class="text-primary"> S/. <span>95.00</span></p><a href="#"
                                                                                                  target="_blank"
                                                                                                  class="btn btn-primary">Comprar </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-wrap-s col-xs-4">
                                    <div class="price-card small">
                                        <div class="extension">.mx</div>
                                        <div class="description">
                                            <p class="text-primary"> S/. <span>95.00</span></p><a href="#"
                                                                                                  target="_blank"
                                                                                                  class="btn btn-primary">Comprar </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-wrap-s col-xs-4">
                                    <div class="price-card small">
                                        <div class="extension">.mx</div>
                                        <div class="description">
                                            <p class="text-primary"> S/. <span>95.00</span></p><a href="#"
                                                                                                  target="_blank"
                                                                                                  class="btn btn-primary">Comprar </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-wrap-s col-xs-4">
                                    <div class="price-card small">
                                        <div class="extension">.mx</div>
                                        <div class="description">
                                            <p class="text-primary"> S/. <span>95.00</span></p><a href="#"
                                                                                                  target="_blank"
                                                                                                  class="btn btn-primary">Comprar </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-wrap-s col-xs-4">
                                    <div class="price-card small">
                                        <div class="extension">.mx</div>
                                        <div class="description">
                                            <p class="text-primary"> S/. <span>95.00</span></p><a href="#"
                                                                                                  target="_blank"
                                                                                                  class="btn btn-primary">Comprar </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-wrap-s col-xs-4">
                                    <div class="price-card small">
                                        <div class="extension">.mx</div>
                                        <div class="description">
                                            <p class="text-primary"> S/. <span>95.00</span></p><a href="#"
                                                                                                  target="_blank"
                                                                                                  class="btn btn-primary">Comprar </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3>MÁS IDEAS DE DOMINIOS</h3>
                    <div class="row">
                        <div class="prefix col-md-4">
                            <h5>PREFIJO</h5>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="extension col-md-4">
                            <h5>EXTENSIÓN</h5>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="suffix col-md-4">
                            <h5>SUFIJO</h5>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                <tr>
                                    <td>dominio.mx</td>
                                    <td>S/. 125.00</td>
                                    <td><a href="#">Agregar +</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <!--Sección Beneficios-->
            <div class="section-benefits">
                <div class="container">
                    <h2>Nuestros Beneficios</h2>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates enim iste provident vitae
                        exercitationem assumenda aliquid ad, culpa sint doloremque voluptatem animi totam rerum velit,
                        maiores non. Unde, amet illum?</p>
                    <div class="row">
                        <div class="media col-md-4">
                            <div class="media-left"><span><img
                                        src="<?php echo get_template_directory_uri() ?>/img/icon_caracteristica.png"
                                        alt="Soporte 24/7"/></span></div>
                            <div class="media-body">
                                <h5>Soporte 24/7</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus atque cupiditate,
                                    numquam laboriosam maxime, consequatur voluptates aspernatur modi repellendus.</p>
                            </div>
                        </div>
                        <div class="media col-md-4">
                            <div class="media-left"><span><img
                                        src="<?php echo get_template_directory_uri() ?>/img/icon_caracteristica.png"
                                        alt="Soporte 24/7"/></span></div>
                            <div class="media-body">
                                <h5>Soporte 24/7</h5>
                                <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                    dantium, totam rem aperiam, eaque ipsa.</p>
                            </div>
                        </div>
                        <div class="media col-md-4">
                            <div class="media-left"><span><img
                                        src="<?php echo get_template_directory_uri() ?>/img/icon_caracteristica.png"
                                        alt="Soporte 24/7"/></span></div>
                            <div class="media-body">
                                <h5>Soporte 24/7</h5>
                                <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                    dantium, totam rem aperiam, eaque ipsa.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="media col-md-4">
                            <div class="media-left"><span><img
                                        src="<?php echo get_template_directory_uri() ?>/img/icon_caracteristica.png"
                                        alt="Soporte 24/7"/></span></div>
                            <div class="media-body">
                                <h5>Soporte 24/7</h5>
                                <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                    dantium, totam rem aperiam, eaque ipsa.</p>
                            </div>
                        </div>
                        <div class="media col-md-4">
                            <div class="media-left"><span><img
                                        src="<?php echo get_template_directory_uri() ?>/img/icon_caracteristica.png"
                                        alt="Soporte 24/7"/></span></div>
                            <div class="media-body">
                                <h5>Soporte 24/7</h5>
                                <p>Texto de relleno: Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                    dantium, totam rem aperiam, eaque ipsa.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Sección Dominios más usados-->
            <div class="more-domains container">
                <h2>DOMINIOS MÁS USADOS</h2>
                <div class="row">
                    <div class="table-wrapper col-md-6">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Dominio</th>
                                <th>Precio</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            </tbody>
                        </table>
                        <span class="igv">IGV incluido *</span>
                    </div>
                    <div class="table-wrapper col-md-6">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Dominio</th>
                                <th>Precio</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            <tr>
                                <td><a href="#">dominio.mx</a></td>
                                <td>S/.125</td>
                            </tr>
                            </tbody>
                        </table>
                        <span class="igv">IGV incluido *</span>
                    </div>
                </div>
            </div>
            <!--Preguntas más frecuentes-->
            <div class="section-faq">
                <div class="container">
                    <h2>PREGUNTAS FRECUENTES</h2>
                    <div class="item-faq">
                        <div id="#heading-1" role="tab">
                            <h5><a href="#collapse-1" role="button" data-toggle="collapse" data-parent="#accordion"
                                   aria-expanded="true" aria-controls="collapse-1">¿Qué es un dominio?</a></h5>
                        </div>
                        <div id="collapse-1" role="tabpanel" aria-labelledby="heading-1" aria-expanded="true"
                             class="collapse in">
                            <div class="panel-body">
                                <p>PAGOS EN LÍNEA: Ud. puede pagar en línea con su Tarjeta de Crédito, mediante su
                                    cuenta Paypal y con su cuenta Moneybookers lo que le permitirá una activación
                                    inmediata:</p>
                                <h5 class="subtitle">SUBTITULO</h5>
                                <ul class="list-circle">
                                    <li>Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</li>
                                    <li>Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</li>
                                    <li>Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.</li>
                                </ul>
                                <h5 class="subtitle">SUBTITULO</h5>
                                <ol class="list-numbered">
                                    <li>lorem ipsum</li>
                                    <li>lorem ipsum</li>
                                    <li>lorem ipsum</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="item-faq">
                        <div id="#heading-2" role="tab">
                            <h5><a href="#collapse-2" role="button" data-toggle="collapse" data-parent="#accordion"
                                   aria-expanded="false" aria-controls="collapse-2" class="collapsed">¿Cómo registro mi
                                    dominio?</a></h5>
                        </div>
                        <div id="collapse-2" role="tabpanel" aria-labelledby="heading-2" aria-expanded="false"
                             class="collapse">
                            <div class="panel-body">
                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo aliquam
                                    inventore ex, placeat obcaecati dolorem blanditiis harum quam qui officia
                                    consequuntur quae laborum, doloremque quaerat nisi impedit exercitationem illum
                                    expedita!</p>
                                <ol class="list-numbered">
                                    <li>lorem ipsum</li>
                                    <li>lorem ipsum</li>
                                    <li>lorem ipsum</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="item-faq">
                        <div id="#heading-3" role="tab">
                            <h5><a href="#collapse-3" role="button" data-toggle="collapse" data-parent="#accordion"
                                   aria-expanded="false" aria-controls="collapse-3" class="collapsed">¿En qué formas
                                    puedo pagar mi dominio?</a></h5>
                        </div>
                        <div id="collapse-3" role="tabpanel" aria-labelledby="heading-3" aria-expanded="false"
                             class="collapse">
                            <div class="panel-body">
                                <p>No se ha agregado ningun contenido</p>
                            </div>
                        </div>
                    </div>
                    <div class="item-faq">
                        <div id="#heading-4" role="tab">
                            <h5><a href="#collapse-4" role="button" data-toggle="collapse" data-parent="#accordion"
                                   aria-expanded="false" aria-controls="collapse-4" class="collapsed">Tengo registrado
                                    mi dominio con otro proveedor. ¿Es factible cambiarme a HOSTING A1 para que sea mi
                                    Registrante?</a></h5>
                        </div>
                        <div id="collapse-4" role="tabpanel" aria-labelledby="heading-4" aria-expanded="false"
                             class="collapse">
                            <div class="panel-body">
                                <ol>
                                    <li> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                                    <li>In nobis ratione eaque autem amet, pariatur obcaecati distinctio vitae sit
                                        labore voluptate aperiam enim voluptatibus
                                    </li>
                                    <li>It dolorum magni deleniti tempora aliquid, recusandae!</li>
                                </ol>
                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. A qui, dolore, neque
                                    placeat ipsa molestias quia aliquid, quibusdam reprehenderit consectetur odit
                                    recusandae repellendus, possimus quos unde. Quis, sint architecto perspiciatis?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-best-of-us">
                <div class="container">
                    <h2>LO MEJOR DE NOSOTROS A TU SERVICIO</h2>
                    <div class="row">
                        <div class="col-md-3 col-sm-6"><img
                                src="<?php echo get_template_directory_uri() ?>/img/icon_nos_1.png">
                            <h5>LOS MEJORES<br>PRECIOS EN OFERTA</h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus
                                eaque ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                        </div>
                        <div class="col-md-3 col-sm-6"><img
                                src="<?php echo get_template_directory_uri() ?>/img/icon_nos_2.png">
                            <h5>SOPORTE <br>PROFESIONAL</h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus
                                eaque ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                        </div>
                        <div class="col-md-3 col-sm-6"><img
                                src="<?php echo get_template_directory_uri() ?>/img/icon_nos_3.png">
                            <h5>GARANTÍA<br>DE SATISFACCIÓN</h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus
                                eaque ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                        </div>
                        <div class="col-md-3 col-sm-6"><img
                                src="<?php echo get_template_directory_uri() ?>/img/icon_nos_4.png">
                            <h5>SEGURIDAD <br>ESPECIALIZADA</h5>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus
                                eaque ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-learning">
                <div class="container">
                    <h2>HOSTING A1 TE ENSEÑA</h2>
                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, perferendis, amet! Numquam
                        tempore deleniti a amet quod minima iure veritatis aliquam expedita. Commodi odit repellendus,
                        officiis est aliquam perferendis. Pariatur.</p><a href="#" class="btn btn-primary">Acceder al
                        centro de aprendizaje</a>
                </div>
            </div>
            <div class="section-way-pay">
                <div class="container"><a href="#" class="btn btn-primary"><i class="fa fa-ticket"> </i>Reporte su pago
                        aquí</a>O elija otras formas de pago<span><a href="#"><img
                                src="<?php echo get_template_directory_uri() ?>/img/icon_pago1.jpg" class="payment"></a><a
                            href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icon_pago2.jpg"
                                          class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri() ?>/img/icon_pago3.jpg" class="payment"></a><a
                            href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icon_pago4.jpg"
                                          class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri() ?>/img/icon_pago1.jpg" class="payment"></a><a
                            href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icon_pago2.jpg"
                                          class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri() ?>/img/icon_pago3.jpg" class="payment"></a><a
                            href="#"><img src="<?php echo get_template_directory_uri() ?>/img/icon_pago4.jpg"
                                          class="payment"></a><a href="#"><img
                                src="<?php echo get_template_directory_uri() ?>/img/icon_pago1.jpg" class="payment"></a></span>
                </div>
            </div>
        </section>
    </div>

<?php get_footer(); ?>