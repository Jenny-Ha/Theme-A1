<?php get_header(); ?>
<!--Main-Wrapper-->
<div class="main-wrapper">
    <!--SECCION INICIO-->
    <section id="pg-home" class="main-content">
        <!--sección de slider principal-->
        <div class="main-slider-section hero">
            <div id="main-slider" data-ride="carousel" class="carousel slide">
                <!-- Indicators-->
                <ol class="carousel-indicators">
                    <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                    <li data-target="#main-slider" data-slide-to="1"></li>
                    <li data-target="#main-slider" data-slide-to="2"></li>
                    <li data-target="#main-slider" data-slide-to="3"></li>
                </ol>
                <!-- Wrapper for slides-->
                <div role="listbox" class="carousel-inner">
                    <div class="slide-01 item active">
                        <div class="container">
                            <div class="wrapper-content">
                                <div class="hero-content">
                                    <h2 class="text-primary">EN SEPTIEMBRE</h2>
                                    <H1> HOSTING WORDPRESS<br><span class="text-primary">50%<small>DE DESCUENTO</small></span>
                                    </H1>
                                    <P>CÓDIGO: AYUDAWP</P>
                                    <p>The atmosphere in Chania has a touch of Florence and Venice. Lorem ipsum dolor
                                        sit amet, consectetur adipisicing elit. </p><a href="#" class="btn btn-primary">
                                        Adquirir ahora</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide-02 item">
                        <div class="container">
                            <div class="wrapper-content">
                                <div class="hero-content">
                                    <h2 class="text-primary">EN SEPTIEMBRE</h2>
                                    <H1> HOSTING WORDPRESS<br><span class="text-primary">50%<small>DE DESCUENTO</small></span>
                                    </H1>
                                    <P>CÓDIGO: AYUDAWP</P>
                                    <p>The atmosphere in Chania has a touch of Florence and Venice. Lorem ipsum dolor
                                        sit amet, consectetur adipisicing elit. </p><a href="#" class="btn btn-primary">
                                        Adquirir ahora</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide-03 item">
                        <div class="container">
                            <div class="wrapper-content">
                                <div class="hero-content">
                                    <h2 class="text-primary">EN SEPTIEMBRE</h2>
                                    <H1> HOSTING WORDPRESS<br><span class="text-primary">50%<small>DE DESCUENTO</small></span>
                                    </H1>
                                    <P>CÓDIGO: AYUDAWP</P>
                                    <p>The atmosphere in Chania has a touch of Florence and Venice. Lorem ipsum dolor
                                        sit amet, consectetur adipisicing elit. </p><a href="#" class="btn btn-primary">
                                        Adquirir ahora</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slide-04 item">
                        <div class="container">
                            <div class="wrapper-content">
                                <div class="hero-content">
                                    <h2 class="text-primary">EN SEPTIEMBRE</h2>
                                    <H1> HOSTING WORDPRESS<br><span class="text-primary">50%<small>DE DESCUENTO</small></span>
                                    </H1>
                                    <P>CÓDIGO: AYUDAWP</P>
                                    <p>The atmosphere in Chania has a touch of Florence and Venice. Lorem ipsum dolor
                                        sit amet, consectetur adipisicing elit. </p><a href="#" class="btn btn-primary">
                                        Adquirir ahora </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Left and right controls--><a href="#main-slider" role="button" data-slide="prev"
                                                  class="left carousel-control"><i aria-hidden="true"
                                                                                   class="icon-prev"></i><span
                        class="sr-only">Previous</span></a><a href="#main-slider" role="button" data-slide="next"
                                                              class="right carousel-control"><i aria-hidden="true"
                                                                                                class="icon-next"></i><span
                        class="sr-only">Next</span></a>
                <!--fin slider-->

            </div>
        </div>
        <!--Buscador de dominios-->
        <div class="section-search-domain">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <form class="search-domain form-inline">
                            <div class="form-group clearfix">
                                <input type="text" placeholder="BUSCA EL NOMBRE DE TU DOMINIO" class="form-control">
                                <input type="submit" value="Buscar Dominio" class="form-control btn btn-default">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <!--Slider de oferta de precios.                 -->
                        <div id="offers-slider" data-ride="carousel" class="carousel slide">
                            <!-- Indicators-->
                            <ol class="carousel-indicators sr-only">
                                <li data-target="#offers-slider" data-slide-to="0" class="active"></li>
                                <li data-target="#offers-slider" data-slide-to="1"></li>
                                <li data-target="#offers-slider" data-slide-to="2"></li>
                            </ol>
                            <!-- Wrapper for slides-->
                            <div role="listbox" class="carousel-inner">
                                <div class="item active">
                                    <p class="intro">Promociones en dominios <span class="text-primary">.com </span></p>
                                    <p class="prices"><span class="before"><small>S/.</small><span
                                                class="tachado">99.00</span></span><span class="offer text-primary"> <small>S/.</small>35.00</span>
                                    </p>
                                </div>
                                <div class="item">
                                    <p class="intro">Promociones en dominios <span class="text-primary">.com </span></p>
                                    <p class="prices"><span class="before"><small>S/.</small><span class="tachado">109.00</span></span><span
                                            class="offer text-primary"> <small>S/.</small>45.00</span></p>
                                </div>
                                <div class="item">
                                    <p class="intro">Promociones en dominios <span class="text-primary">.com </span></p>
                                    <p class="prices"><span class="before"><small>S/.</small><span class="tachado">159.00</span></span><span
                                            class="offer text-primary"> <small>S/.</small>65.00</span></p>
                                </div>
                            </div>
                            <!-- Left and right controls--><a href="#offers-slider" role="button" data-slide="prev"
                                                              class="left carousel-control"><i aria-hidden="true"
                                                                                               class="icon-prev"></i><span
                                    class="sr-only">Previous</span></a><a href="#offers-slider" role="button"
                                                                          data-slide="next"
                                                                          class="right carousel-control"><i
                                    aria-hidden="true" class="icon-next"></i><span class="sr-only">Next</span></a>
                        </div>
                        <!--Fin de slider-->
                    </div>
                </div>
            </div>
        </div>
        <!--Sección de Negocios-Hosting-->
        <div class="section-business-hosting clearfix">
            <div class="standard-hosting">
                <h2>EL MEJOR PLAN HOSTING<br>ADECUADO PARA TI</h2>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                    totam rem aperiam.</p>
                <h5>HOSTING ESTÁNDAR</h5>
                <P>Desde S/.82.25 x Año<a href="#" class="btn btn-primary">Adquierelo ya</a></P>
            </div>
            <div class="hosting-reseller">
                <h2>HAGAMOS NEGOCIOS<br>SEAMOS ALIADOS</h2>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                    totam rem aperiam.</p>
                <h5>HOSTING RESELLER</h5>
                <P><a href="#" class="btn btn-default">Empezar ahora</a>Desde S/.51.63 x Mes</P>
            </div>
        </div>
        <!--Sección Hosting WORDPRESS-->
        <div class="section-wordpress-hosting">
            <div class="container">
                <h2>LA MANERA MAS RÁPIDA DE COMENZAR UN BLOG</h2>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                    totam rem aperiam, eaque ipsa quae ab illo.</p>
                <div><img src="<?php echo get_template_directory_uri()?>/img/WP_logo.jpg" alt="Hosting Wordpress con Hosting A1">
                    <div class="buy-plan">
                        <div class="price-plan btn-group">
                            <button type="button" class="btn btn-input"><span class="text-primary">1 año </span>-
                                S/.<span class="text-primary">14.00 </span>al mes
                            </button>
                            <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                    class="btn btn-primary dropdown-toggle"><span class="caret"></span><span
                                    class="sr-only">Toggle Dropdown</span></button>
                            <ul class="list-price dropdown-menu">
                                <li><a href="#"><span class="text-primary">1 año </span>- S/.<span class="text-primary">14.00 </span>al
                                        mes</a></li>
                                <li><a href="#"><span class="text-primary">2 años </span>- S/.<span
                                            class="text-primary">12.00 </span>al mes</a></li>
                            </ul>
                            <span class="igv">IGV incluido*</span>
                        </div>
                        <a href="#" class="btn btn-primary"><i class="fa fa-shopping-cart"> </i> ADQUIRIR</a>
                    </div>
                </div>
            </div>
        </div>
        <!--Sección Plan VPS-->
        <div class="section-plan-vps">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8">
                        <h2>¿POR QUÉ PASARSE A CLOUD VPS?</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                            laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                            architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                            aspernatur aut odit aut fugit.</p>
                        <div class="row benefits-vps">
                            <div class="media col-md-4 col-xs-6">
                                <div class="media-left"><span><img src="<?php echo get_template_directory_uri()?>/img/icon_caracteristica.png"
                                                                   alt="Soporte 24/7"></span></div>
                                <div class="media-body media-middle">
                                    <h5>Soporte 24/7</h5>
                                </div>
                            </div>
                            <div class="media col-md-4 col-xs-6">
                                <div class="media-left"><span><img src="<?php echo get_template_directory_uri()?>/img/icon_caracteristica.png" alt="Sencillo"></span>
                                </div>
                                <div class="media-body media-middle">
                                    <h5>Sencillo</h5>
                                </div>
                            </div>
                            <div class="media col-md-4 col-xs-6">
                                <div class="media-left"><span><img src="<?php echo get_template_directory_uri()?>/img/icon_caracteristica.png" alt="Escalable"></span>
                                </div>
                                <div class="media-body media-middle">
                                    <h5>Escalable</h5>
                                </div>
                            </div>
                            <div class="media col-md-4 col-xs-6">
                                <div class="media-left"><span><img src="<?php echo get_template_directory_uri()?>/img/icon_caracteristica.png" alt="Comunidad"></span>
                                </div>
                                <div class="media-body media-middle">
                                    <h5>Comunidad</h5>
                                </div>
                            </div>
                            <div class="media col-md-4 col-xs-6">
                                <div class="media-left"><span><img src="<?php echo get_template_directory_uri()?>/img/icon_caracteristica.png"
                                                                   alt="Disponible"></span></div>
                                <div class="media-body media-middle">
                                    <h5>Disponible</h5>
                                </div>
                            </div>
                            <div class="media col-md-4 col-xs-6">
                                <div class="media-left"><span><img src="<?php echo get_template_directory_uri()?>/img/icon_caracteristica.png"
                                                                   alt="Tecnología"></span></div>
                                <div class="media-body media-middle">
                                    <h5>Tecnología</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="wrap-ad">
                            <h3>QUE ESPERAS PARA CONSEGUIR EL MEJOR <br><span>VPS</span></h3>
                            <div class="info bg-white">No pierdas la oportunidad de tener el control de todo.</div>
                            <a href="#" class="btn btn-primary">CONSEGUIR UN PLAN</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Sección de Testimonios-->
        <div class="section-testimonials">
            <div class="container">
                <h2>POR QUE LOS CLIENTES NOS PREFIEREN</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore</p>
                <!--Slider de testimonios-->
                <div class="testimonials-slider">
                    <div class="testimonial-01">
                        <div class="image">
                            <div class="image-box"><img src="<?php echo get_template_directory_uri()?>/img/testimonials/team3.jpg"></div>
                        </div>
                        <div class="testimonial-info">
                            <p class="testimonial-content">
                                <small class="comma">“</small>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s.
                            </p>
                            <span class="author">WALTHER AYALA </span>
                            <small>-</small>
                            <span class="employment">UX/UI Designer</span>
                        </div>
                    </div>
                    <div class="testimonial-02">
                        <div class="image">
                            <div class="image-box"><img src="<?php echo get_template_directory_uri()?>/img/testimonials/team2.png"></div>
                        </div>
                        <div class="testimonial-info">
                            <p class="testimonial-content">
                                <small class="comma">“</small>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s.
                            </p>
                            <span class="author">WALTHER AYALA </span>
                            <small>-</small>
                            <span class="employment">UX/UI Designer</span>
                        </div>
                    </div>
                    <div class="testimonial-03">
                        <div class="image">
                            <div class="image-box"><img src="<?php echo get_template_directory_uri()?>/img/testimonials/team3.jpg"></div>
                        </div>
                        <div class="testimonial-info">
                            <p class="testimonial-content">
                                <small class="comma">“</small>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s.
                            </p>
                            <span class="author">SAM GOOD </span>
                            <small>-</small>
                            <span class="employment">UX/UI Designer</span>
                        </div>
                    </div>
                    <div class="testimonial-04">
                        <div class="image">
                            <div class="image-box"><img src="<?php echo get_template_directory_uri()?>/img/testimonials/team4.jpg"></div>
                        </div>
                        <div class="testimonial-info">
                            <p class="testimonial-content">
                                <small class="comma">“</small>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s.
                            </p>
                            <span class="author">SAM GOOD </span>
                            <small>-</small>
                            <span class="employment">UX/UI Designer</span>
                        </div>
                    </div>
                    <div class="testimonial-05">
                        <div class="image">
                            <div class="image-box"><img src="<?php echo get_template_directory_uri()?>/img/testimonials/team5.jpg"></div>
                        </div>
                        <div class="testimonial-info">
                            <p class="testimonial-content">
                                <small class="comma">“</small>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s.
                            </p>
                            <span class="author">SAM GOOD  </span>
                            <small>-</small>
                            <span class="employment">UX/UI Designer</span>
                        </div>
                    </div>
                    <div class="testimonial-06">
                        <div class="image">
                            <div class="image-box"><img src="<?php echo get_template_directory_uri()?>/img/testimonials/team1.jpg"></div>
                        </div>
                        <div class="testimonial-info">
                            <p class="testimonial-content">
                                <small class="comma">“</small>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s.
                            </p>
                            <span class="author">SAM GOOD  </span>
                            <small>-</small>
                            <span class="employment">UX/UI Designer</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-best-of-us gray">
            <div class="container">
                <h2>LO MEJOR DE NOSOTROS A TU SERVICIO</h2>
                <div class="row">
                    <div class="col-md-3 col-sm-6"><img src="<?php echo get_template_directory_uri()?>/img/icon_nos_1.png">
                        <h5>LOS MEJORES<br>PRECIOS EN OFERTA</h5>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus eaque
                            ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                    </div>
                    <div class="col-md-3 col-sm-6"><img src="<?php echo get_template_directory_uri()?>/img/icon_nos_2.png">
                        <h5>SOPORTE <br>PROFESIONAL</h5>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus eaque
                            ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                    </div>
                    <div class="col-md-3 col-sm-6"><img src="<?php echo get_template_directory_uri()?>/img/icon_nos_3.png">
                        <h5>GARANTÍA<br>DE SATISFACCIÓN</h5>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus eaque
                            ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                    </div>
                    <div class="col-md-3 col-sm-6"><img src="<?php echo get_template_directory_uri()?>/img/icon_nos_4.png">
                        <h5>SEGURIDAD <br>ESPECIALIZADA</h5>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas eius necessitatibus eaque
                            ipsum exercitationem possimus asperiores libero impedit rerum quo officia.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--Sección Slider de Frases-->
        <div class="section-quote">
            <!--Slider de oferta de precios-->
            <div class="container">
                <div id="text-slider" data-ride="carousel" class="carousel slide">
                    <!-- Indicators-->
                    <ol class="carousel-indicators sr-only">
                        <li data-target="#text-slider" data-slide-to="0" class="active"></li>
                        <li data-target="#text-slider" data-slide-to="1"></li>
                        <li data-target="#text-slider" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides-->
                    <div role="listbox" class="carousel-inner">
                        <div class="item active">
                            <h3>“BRINDANDO HOSTING PARA EMPRESAS POR MÁS DE 10AÑOS” </h3>
                        </div>
                        <div class="item">
                            <h3>“Lorem ipsum dolor sit amet, consectetur adipisicing elit".</h3>
                        </div>
                        <div class="item">
                            <h3>"Dolor nostrum adipisci amet totam asperiores similique".</h3>
                        </div>
                    </div>
                    <!-- Left and right controls--><a href="#text-slider" role="button" data-slide="prev"
                                                      class="left carousel-control"><i aria-hidden="true"
                                                                                       class="icon-prev"></i><span
                            class="sr-only">Previous</span></a><a href="#text-slider" role="button" data-slide="next"
                                                                  class="right carousel-control"><i aria-hidden="true"
                                                                                                    class="icon-next"></i><span
                            class="sr-only">Next</span></a>
                </div>
                <!--Fin de slider-->
            </div>
        </div>
        <div class="section-learning">
            <div class="container">
                <h2>HOSTING A1 TE ENSEÑA</h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, perferendis, amet! Numquam tempore
                    deleniti a amet quod minima iure veritatis aliquam expedita. Commodi odit repellendus, officiis est
                    aliquam perferendis. Pariatur.</p><a href="/zona-aprendizaje/" class="btn btn-primary">Acceder al centro de
                    aprendizaje</a>
            </div>
        </div>
        <div class="section-way-pay">
            <div class="container"><a href="#" class="btn btn-primary"><i class="fa fa-ticket"> </i>Reporte su pago aquí</a>O
                elija otras formas de pago<span><a href="#"><img src="<?php echo get_template_directory_uri()?>/img/icon_pago1.jpg" class="payment"></a><a
                        href="#"><img src="<?php echo get_template_directory_uri()?>/img/icon_pago2.jpg" class="payment"></a><a href="#"><img
                            src="<?php echo get_template_directory_uri()?>/img/icon_pago3.jpg" class="payment"></a><a href="#"><img src="<?php echo get_template_directory_uri()?>/img/icon_pago4.jpg"
                                                                                             class="payment"></a><a
                        href="#"><img src="<?php echo get_template_directory_uri()?>/img/icon_pago1.jpg" class="payment"></a><a href="#"><img
                            src="<?php echo get_template_directory_uri()?>/img/icon_pago2.jpg" class="payment"></a><a href="#"><img src="<?php echo get_template_directory_uri()?>/img/icon_pago3.jpg"
                                                                                             class="payment"></a><a
                        href="#"><img src="<?php echo get_template_directory_uri()?>/img/icon_pago4.jpg" class="payment"></a><a href="#"><img
                            src="<?php echo get_template_directory_uri()?>/img/icon_pago1.jpg" class="payment"></a></span></div>
        </div>
    </section>
</div>
<?php get_footer(); ?>