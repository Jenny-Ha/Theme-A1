<?php
/*
Template Name: For Domains
*/
get_header();

if (isset($_GET['keyword'])):
    ?>
    <style>
        .hide-on-keyword {
            display: none;
        }

        .show-on-keyword {
            display: inherit;
        }
    </style>
<?php else: ?>
    <style>
        .hide-on-keyword {
            display: inherit;
        }

        .show-on-keyword {
            display: none;
        }
    </style>
<?php endif; ?>

<div class="main-wrapper">
    <section id="pg-dominios" class="main-content">
        <?php while (have_posts()) :
            the_post();
            the_content();
            ?>
        <?php endwhile; ?>
    </section>
</div>

<script type="text/javascript">
var keyword = '<?php echo $_GET['keyword']; ?>';
</script>
<?php get_footer(); ?>
