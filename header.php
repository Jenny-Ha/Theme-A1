<?php 
set_wc_country();
global $WOOCS;
$currency = $WOOCS->current_currency;
$local_currency = get_currency_in_currencies_available();
$current_country = get_country();
$countries = get_allowed_countries();
?>
<!DOCTYPE html>
<html lang="es-PE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <!-- Titulo-->
    <title>HOSTING A1 :: Empresa de Hosting, Alojamiento de Páginas Web en América Latina y Registro de Dominios en Internet</title>
    <!-- Links-->
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <!--    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Muli:300,300i,400" rel="stylesheet">-->
<script type="text/javascript">
    var site_url = '<?php echo get_site_url();?>';
    var template_url = '<?php echo get_template_directory_uri();?>';
</script>
</head>
<body>
<!--header-->
<header id="header">
    <!-- Top bar-->
    <div class="top-bar-container container">
        <div class="top-bar row">
          <div class="col-md-3 col-sm-4 col-xs-5">
                <div id="menu-pais" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle"><span
                            title="<?php echo cleanString($current_country); ?>"
                            class="current"></span><?php echo $current_country ?><span class="caret"></span></a>
                    <ul class="dropdown-menu paises">   
                        <?php foreach ($countries as $k => $v) {
                         echo '<li ><a href ="?country='.$k.'" ><span title = "'.cleanString($v).'" ></span >'.$v.'</a ></li >';
                        } ?>

                        <li><a href="?country=otros"><span title="Otros"></span>Otros Paises</a></li>
                    </ul>
                </div>
                <!--Selección de moneda-->
                <div id="menu-moneda" class="dropdown"><a data-toggle="dropdown" class="m-local dropdown-toggle">
                        <?php if ($local_currency['name'] == $currency):
                            echo $local_currency['name'] . ' ' . $local_currency['symbol'];
                        else:
                            echo 'USD $';
                        endif;
                        ?>
                        <?php if ($local_currency): ?>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu moneda">
                        <li><a href="?currency=<?php echo $local_currency['name'] ?>" class="m-local">
                                <?php echo $local_currency['name'] . ' ' . $local_currency['symbol'] ?>
                            </a></li>
                        <li><a href="?currency=USD">USD $</a></li>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-7">
                <?php clean_custom_menu("top_menu","menu-topbar","hidden-sm hidden-xs") ?>
                <!-- <ul class="menu-topbar"> -->
                    <!-- <li><i aria-hidden="true" class="fa fa-phone"></i>Llámanos: 065 - 123 456 789</li>
                    <li class="hidden"><i aria-hidden="true" class="fa fa-envelope"></i>Escríbenos: info@hostinga1.com</li>
                    <li class="hidden-sm hidden-xs"><a href="nosotros.html">Nosotros</a></li>
                    <li class="hidden-sm hidden-xs"><a href="soporte.html">Soporte</a></li>
                    <li class="hidden-sm hidden-xs"><a href="contacto.html">Contacto</a></li>
                    <li><a href="#"><i aria-hidden="true" class="fa fa-comment"></i>Chat</a></li>
                    <li class="hidden-sm hidden-xs"><a href="#"><i aria-hidden="true" class="fa fa-users"></i>Acceso Clientes</a></li> -->
                <!-- </ul> -->
            </div>
        </div>
    </div>
    <!-- Seccion Head-->
    <div class="head clearfix">
        <!-- Menu de navegación-->
        <nav class="navbar container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#main-menu" class="navbar-toggle"><span
                            class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand"><img id="logo"
                                                          src="<?php echo esc_url(get_template_directory_uri()); ?>/img/logo_a1.png"
                                                          alt="Hosting A1"></a>
                </div>
                <div id="main-menu" class="collapse navbar-collapse">
                    <ul class="top-menu nav navbar-nav navbar-right">
                        <li class="active"><a href="<?php echo a1_get_url('inicio'); ?>">Inicio</a></li>
                        <!-- Menú multinivel DOMINIOS-->
                        <li class="menu-dominios"><a href="#">Dominios</a>
                            <div class="menu-container">
                                <div class="container clearfix">
                                    <ul class="pull-left">
                                        <li><a href="<?php echo a1_get_url('comprar-dominios');  ?>" title="Registrar dominio">
                                               <i aria-hidden="true" class="fa fa-chevron-right"></i> 
                                                    REGISTRAR TU DOMINIO <br><span>Pon tu marca en internet ya mismo</span></a></li>
                                        <li><a href="<?php echo a1_get_url('transferir-dominio'); ?>" title="Transferir Dominio"><i
                                                    aria-hidden="true" class="fa fa-chevron-right"></i> TRANSFERIR
                                                DOMINIO <br><span>En sencillos pasos</span></a></li>
                                    </ul>
                                    <ul class="description pull-left">
                                        <li>
                                            <h5 class="text-primary">GRATIS CON TU DOMINO</h5>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                eiusmod.</p>
                                            <p class="text-primary"><span><i aria-hidden="true"
                                                                             class="fa fa-check-circle"></i> GESTIÓN DNS</span><span><i
                                                        aria-hidden="true" class="fa fa-check-circle"></i> CONTROL PANEL</span><span><i
                                                        aria-hidden="true" class="fa fa-check-circle"></i> SOPORTE 24/7</span>
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li><a href="<?php echo a1_get_url('pagina-web-con-wordpress'); ?>" title="Páginas web con Wordpress">Páginas Web</a></li>
                        <!-- Menú Multinivel HOSTING-->
                        <li class="menu-hosting"><a href="#">Hosting</a>
                            <div class="menu-container">
                                <div class="container clearfix">
                                    <ul class="two-column">
                                        <li><a href="<?php echo  a1_get_url('barato'); ?>" title="Hosting web"><i aria-hidden="true"
                                                                                                    class="fa fa-chevron-right"></i>HOSTING
                                                ECONÓMICO <br><span>Con todas las herramientas que necesitas.</span></a></li>
                                        <li><a href="<?php echo a1_get_url('web-empresa'); ?>" title="Hosting empresa"><i
                                                    aria-hidden="true" class="fa fa-chevron-right"></i>HOSTING EMPRESA
                                                <br><span>Con toda la tecnología cloud.</span></a></li>
                                        <li><a href="<?php echo a1_get_url('wordpress'); ?>" title="Hosting wordpress"><i
                                                    aria-hidden="true" class="fa fa-chevron-right"></i>HOSTING WORDPRESS
                                                <br><span>Wordpress al instante.</span></a></li>
                                        <!--ul.pull-left-->
                                        <li><a href="<?php echo a1_get_url('asp-net'); ?>" title="Hosting ASP.NET"><i
                                                    aria-hidden="true" class="fa fa-chevron-right"></i>HOSTING ASP.NET
                                                <br><span>Más MSSQL.</span></a></li>
                                        <li><a href="<?php echo a1_get_url('reseller'); ?>" title="Hosting wordpress"><i
                                                    aria-hidden="true" class="fa fa-chevron-right"></i>HOSTING
                                                RESELLER<br><span>Inicia tu negocio.</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li><a href="<?php echo a1_get_url('servidores-virtuales');?>" title="Servidores">Servidores</a></li>
                        <li><a href="<?php echo a1_get_url('certificados-ssl');?>" title="Seguridad">Seguridad</a></li>
                        <li><a href="<?php echo get_site_url();?>/blog/" title="Blog">Blog</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>