<?php

require_once(__DIR__.'/includes/domainer/domainer.php');


function set_wc_country(){
    global $WOOCS;
    $local_country = $_GET['country'];
    
    // $country_code = $WOOCS->storage->get_val('woocs_user_country');

    if (isset($local_country)) {
        $country_code = $WOOCS->storage->get_val('woocs_user_country');
        if ($country_code != $local_country) {                
            $WOOCS->storage->set_val('woocs_user_country', strtoupper($local_country));        
            $local_currency = $_GET['currency'];
            if (!isset($local_currency)) {
                $currency = $WOOCS->get_currency_by_country(strtoupper($local_country));
                if ($currency == null) {
                    $currency = 'USD';
                }
                $WOOCS->storage->set_val('woocs_current_currency', $currency);
                $pattern = get_possible_path();

                 header('Location: ' . a1_get_url($pattern));
                 exit();
            }
        }
    }
}


//$WOOCS->storage->set_val('xwoocs_user_country', $local_country);

function a1host_scripts()
{
    // Add custom fonts, used in the main stylesheet.
//    wp_enqueue_style('twentyfifteen-fonts', twentyfifteen_fonts_url(), array(), null);

    // Add Genericons, used in the main stylesheet.
    wp_enqueue_style('style', get_template_directory_uri() . '/css/style.css', array(), '3.2');
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.css', array(), '3.2');
    wp_enqueue_style('fonts-google', 'https://fonts.googleapis.com/css?family=Montserrat:400,700|Muli:300,300i,400', array(), '3.2');
    wp_enqueue_style('jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', array(), '3.2');

    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', true, null);
    wp_enqueue_script('jquery');

    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1', true);

    wp_register_script('domainer',  get_template_directory_uri() . '/includes/domainer/domainer.js', array(), '1',true);
    wp_enqueue_script('domainer');

}

add_action('wp_enqueue_scripts', 'a1host_scripts');

add_theme_support('post-thumbnails');
add_theme_support('menus');
add_theme_support('widgets');

function register_my_menus() {
  register_nav_menus(
    array(
      'top_menu' => __( 'Top Menu' ),
      'principal_menu' => __( 'Principal Menu' ),
      'footer_menu' => __( 'Footer Menu' ),
      'left_menu' => __( 'Left Menu' )
    )
  );
}

add_action( 'init', 'register_my_menus' );

add_action('widgets_init', 'widgets_init');
function widgets_init()
{
    register_sidebar(array(
        'name' => __('Sidebar', 'a1host'),
        'id' => 'sidebar-1',
        'description' => __('Add widgets here to appear in your sidebar.', 'twentysixteen'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => __('Post Sidebar', 'post-sidebar'),
        'id' => 'post-sidebar',
        'description' => __('Widgets in this area will be shown on all posts and pages.', 'theme-slug'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
        'class' => 'register_sidebar'
    ));

    register_sidebar(array(
        'name' => __('Footer 1 Bar', 'footer-1-bar'),
        'id' => 'footer-1-bar',
        'description' => __('Widgets in this area will be shown on all posts and pages.', 'theme-slug'),
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<p class="title-1">',
        'after_title' => '</p>'
    ));
    register_sidebar(array(
        'name' => __('Footer 2 Bar', 'footer-2-bar'),
        'id' => 'footer-2-bar',
        'description' => __('Widgets in this area will be shown on all posts and pages.', 'theme-slug'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
        'class' => 'register_sidebar'
    ));
}

function clean_custom_menu($theme_location, $ul_class, $li_class)
{
    if (($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location])) {
        $menu = get_term($locations[$theme_location], 'nav_menu');
        $menu_items = wp_get_nav_menu_items($menu->term_id);

        $menu_list = '<nav>' . "\n";
        $menu_list .= '<ul class="' . $ul_class . '">' . "\n";

        $count = 0;
        $submenu = false;

        foreach ($menu_items as $menu_item) {

            $link = $menu_item->url;
            $title = $menu_item->title;

            if (!$menu_item->menu_item_parent) {
                $parent_id = $menu_item->ID;

                $menu_list .= '<li class="' . $li_class . '">' . "\n";
                $menu_list .= '<a href="' . $link . '" class="title">' . $title . '</a>' . "\n";
            }

            if ($parent_id == $menu_item->menu_item_parent) {

                if (!$submenu) {
                    $submenu = true;
                    $menu_list .= '<ul class="sub-menu">' . "\n";
                }

                $menu_list .= '<li class="item">' . "\n";
                $menu_list .= '<a href="' . $link . '" class="title">' . $title . '</a>' . "\n";
                $menu_list .= '</li>' . "\n";


                if ($menu_items[$count + 1]->menu_item_parent != $parent_id && $submenu) {
                    $menu_list .= '</ul>' . "\n";
                    $submenu = false;
                }

            }

            if ($menu_items[$count + 1]->menu_item_parent != $parent_id) {
                $menu_list .= '</li>' . "\n";
                $submenu = false;
            }

            $count++;
        }

        $menu_list .= '</ul>' . "\n";
        $menu_list .= '</nav>' . "\n";

    } else {
        $menu_list = '<!-- no menu defined in location "' . $theme_location . '" -->';
    }
    echo $menu_list;
}

function display_twitter_element()
{
    ?>
    <input type="text" name="twitter_url" id="twitter_url" value="<?php echo get_option('twitter_url'); ?>"/>
    <?php
}

function display_facebook_element()
{
    ?>
    <input type="text" name="facebook_url" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>"/>
    <?php
}

function display_phone_element()
{
    ?>
    <input type="text" name="phone_number" id="phone_number" value="<?php echo get_option('phone_number'); ?>"/>
    <?php
}

function display_email()
{
    ?>
    <input type="text" name="email" id="email" value="<?php echo get_option('email'); ?>"/>
    <?php
}

function display_link_text()
{
    ?>
    <input type="text" name="link_text" id="link_text" value="<?php echo get_option('link_text'); ?>"/>
    <?php
}

function handle_file_upload($option)
{
    if (!empty($_FILES["image_file"]["tmp_name"])) {
        $urls = wp_handle_upload($_FILES["image_file"], array('test_form' => FALSE));
        $temp = $urls["url"];
        return $temp;
    }

    return $option;
}

function display_image_file()
{
    ?>
    <input type="file" name="image_file"/>
    <?php echo get_option('image_file'); ?>
    <?php
}

function display_theme_panel_fields()
{
    add_settings_section("section", "All Settings", null, "theme-options");

    add_settings_field("twitter_url", "Twitter Url", "display_twitter_element", "theme-options", "section");
    add_settings_field("facebook_url", "Facebook Url", "display_facebook_element", "theme-options", "section");
    add_settings_field("display_phone_number", "Número Telefonico", "display_phone_element", "theme-options", "section");
    add_settings_field("display_email", "Correo Electrónico", "display_email", "theme-options", "section");
    add_settings_field("display_link_text", "Link Text", "display_link_text", "theme-options", "section");
//    add_settings_field("demo_file_display", "Link Image", "display_image_file", "theme-options", "section");


    register_setting("section", "twitter_url");
    register_setting("section", "facebook_url");
    register_setting("section", "phone_number");
    register_setting("section", "email");
    register_setting("section", "link_text");
//    register_setting("section", "image_file","handle_file_upload");
}


function display_server_vps_1()
{
    ?>
    <input type="text" name="server_vps_1" id="server_vps_1" value="<?php echo get_option('server_vps_1'); ?>"/>
    <?php
}

function display_server_vps_2()
{
    ?>
    <input type="text" name="server_vps_2" id="server_vps_2" value="<?php echo get_option('server_vps_2'); ?>"/>
    <?php
}

function display_server_vps_3()
{
    ?>
    <input type="text" name="server_vps_3" id="server_vps_3" value="<?php echo get_option('server_vps_3'); ?>"/>
    <?php
}

function display_server_wp_1()
{
    ?>
    <input type="text" name="server_wp_1" id="server_wp_1" value="<?php echo get_option('server_wp_1'); ?>"/>
    <?php
}

function display_server_wp_2()
{
    ?>
    <input type="text" name="server_wp_2" id="server_wp_2" value="<?php echo get_option('server_wp_2'); ?>"/>
    <?php
}

function display_server_wp_3()
{
    ?>
    <input type="text" name="server_wp_3" id="server_wp_3" value="<?php echo get_option('server_wp_3'); ?>"/>
    <?php
}

function display_theme_panel_server_products()
{
    add_settings_section("section", "Servers Options", null, "theme-options-server");

    add_settings_field("server_vps_1", "Server VPS 1", "display_server_vps_1", "theme-options-server", "section");
    register_setting("section", "server_vps_1");

    add_settings_field("server_vps_2", "Server VPS 2", "display_server_vps_2", "theme-options-server", "section");
    register_setting("section", "server_vps_2");

    add_settings_field("server_vps_3", "Server VPS 3", "display_server_vps_3", "theme-options-server", "section");
    register_setting("section", "server_vps_3");


    add_settings_field("server_wp_1", "Server WP 1", "display_server_wp_1", "theme-options-server", "section");
    register_setting("section", "server_wp_1");

    add_settings_field("server_wp_2", "Server WP 2", "display_server_wp_2", "theme-options-server", "section");
    register_setting("section", "server_wp_2");

    add_settings_field("server_wp_3", "Server WP 3", "display_server_wp_3", "theme-options-server", "section");
    register_setting("section", "server_wp_3");
}


function display_domain_promo_1()
{
    ?>
    <input type="text" name="domain_promo_1" id="domain_promo_1" value="<?php echo get_option('domain_promo_1'); ?>"/>
    <?php
}

function display_domain_promo_2()
{
    ?>
    <input type="text" name="domain_promo_2" id="domain_promo_2" value="<?php echo get_option('domain_promo_2'); ?>"/>
    <?php
}

function display_domain_promo_3()
{
    ?>
    <input type="text" name="domain_promo_3" id="domain_promo_3" value="<?php echo get_option('domain_promo_3'); ?>"/>
    <?php
}

function display_theme_panel_domain_promo()
{
    add_settings_section("section", "Domain Options", null, "theme-options-domain");

    add_settings_field("domain_promo_1", "Domain Promo 1", "display_domain_promo_1", "theme-options-domain", "section");
    register_setting("section", "domain_promo_1");

    add_settings_field("domain_promo_2", "Domain Promo 2", "display_domain_promo_2", "theme-options-domain", "section");
    register_setting("section", "domain_promo_2");

    add_settings_field("domain_promo_3", "Domain Promo 3", "display_domain_promo_3", "theme-options-domain", "section");
    register_setting("section", "domain_promo_3");
}

add_action("admin_init", "display_theme_panel_fields");
add_action("admin_init", "display_theme_panel_server_products");
add_action("admin_init", "display_theme_panel_domain_promo");

function theme_settings_page()
{
    ?>
    <div class="wrap">
        <h1>Theme Panel</h1>
        <form method="post" action="options.php">
            <?php
            settings_fields("section");
            do_settings_sections("theme-options");
            echo '<hr/>';
            do_settings_sections("theme-options-server");
            echo '<hr/>';
            do_settings_sections("theme-options-domain");
            submit_button();
            ?>
        </form>
    </div>
    <?php
}

function add_theme_menu_item()
{
    add_menu_page("A1 Settings", "A1 Settings", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");


function a1_the_content($more_link_text = null, $strip_teaser = false)
{
    $content = get_the_content($more_link_text, $strip_teaser);

    /**
     * Filters the post content.
     *
     * @since 0.71
     *
     * @param string $content Content of the current post.
     */
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    $content = str_replace('template_path', get_template_directory_uri(), $content);
    echo $content;
}

function get_country_code(){
    $pd = WC_Geolocation::geolocate_ip();
    return $pd['country'];
}

function get_country($country_code = null)
{
    global $WOOCS;
    if (is_null($country_code)||$country_code=='') {
        $country_code = $WOOCS->storage->get_val('woocs_user_country');
        if (is_null($country_code)||$country_code=='') {            
            $country_code = get_country_code();
            $WOOCS->storage->set_val('woocs_user_country', $pd['country']);        
        }
    }
    $countries_obj = new WC_Countries();
    $countries = $countries_obj->__get('countries');
    $country = $countries[$country_code];

    return $country;
}

function get_country_clear($country_code = null)
{

    $country = cleanString(get_country($country_code));
    return $country;

}

function a1_get_url($pattern)
{
    global $WOOCS;
    $country_code = $WOOCS->storage->get_val('woocs_user_country');
    $country_name = strtolower(get_country_clear($country_code));
//    if ($country_name == null) {
//        return $pattern;
//    }
    $page_path = $pattern . '-' . $country_name;
    $page = get_page_by_path($page_path);
    if ($page) {
        return get_site_url() . '/' . $page_path;
    } else {
        return get_site_url() . '/' . $pattern;
    }


}

function cleanString($text)
{
    $utf8 = array(
        '/[áàâãªä]/u' => 'a',
        '/[ÁÀÂÃÄ]/u' => 'A',
        '/[ÍÌÎÏ]/u' => 'I',
        '/[íìîï]/u' => 'i',
        '/[éèêë]/u' => 'e',
        '/[ÉÈÊË]/u' => 'E',
        '/[óòôõºö]/u' => 'o',
        '/[ÓÒÔÕÖ]/u' => 'O',
        '/[úùûü]/u' => 'u',
        '/[ÚÙÛÜ]/u' => 'U',
        '/ç/' => 'c',
        '/Ç/' => 'C',
        '/ñ/' => 'n',
        '/Ñ/' => 'N',
        '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u' => ' ', // Literally a single quote
        '/[“”«»„]/u' => ' ', // Double quote
        '/ /' => ' ', // nonbreaking space (equiv. to 0x160)
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}


function show_server_vps_product($option, $extra_class = '')
{
    $id = (int)get_option('server_vps_' . $option);
    $_pf = new WC_Product_Factory();
    $_product = $_pf->get_product($id);
    if ($_product) {
        echo '<div class="vps-plan ' . $extra_class . '">
        <div class="features">
            <h3 class="text-center">' . $_product->get_title() . '</h3>
            ' . $_product->post->post_content . '
        </div>
        <div class="price text-center"><span class="igv">IGV incluido* </span>
            <p class="text-primary">' . wc_price($_product->get_price()) . '</p>
            <h5>X MES </h5><a href="#" class="btn btn-primary">Configurar</a>
        </div>
    </div>';
    }
}

function show_server_vps_product_by_id($id, $extra_class = '')
{
    $_pf = new WC_Product_Factory();
    $_product = $_pf->get_product($id);
    if ($_product) {
        echo '<div class="vps-plan ' . $extra_class . '">
        <div class="features">
            <h3 class="text-center">' . $_product->get_title() . '</h3>
            ' . $_product->post->post_content . '
        </div>
        <div class="price text-center"><span class="igv">IGV incluido* </span>
            <p class="text-primary">' . wc_price($_product->get_price()) . '</p>
            <h5>X MES </h5><a href="#" class="btn btn-primary">Configurar</a>
        </div>
    </div>';
    }
}

function show_server_vps_product_item($option)
{
    $id = (int)get_option('server_vps_' . $option);
    $_pf = new WC_Product_Factory();
    $_product = $_pf->get_product($id);
    if ($_product) {
        echo '<span class="text-primary">' . $_product->post->post_excerpt . ' </span>- ' . wc_price($_product->get_price()) . ' al mes';
    }
}


function show_domain_promo_item($option, $extra_class = '')
{
    $id = (int)get_option('domain_promo_' . $option);
    $_pf = new WC_Product_Factory();
    $_product = $_pf->get_product($id);
    if ($_product) {
        $price = get_rounded_price($_product->get_price());           
        $regular_price = get_rounded_price($_product->get_regular_price());
        $currency_simbol = get_woocommerce_currency_symbol($currency);

        $has_offer = has_offer($_product);
        $show_igv = show_igv();

        
        $result = '<div class="item ' . $extra_class . '"><p class="intro">' . $_product->post->post_content . '</p><p class="prices">';

        if($has_offer){
            $result .= '<span class="before"><small>'.$currency_simbol.'</small><span class="tachado">' . $regular_price . '</span></span>';
        }

        $result .= '<span class="offer text-primary"> <small>'.$currency_simbol.'</small>' . $price . '</span>';

        if($show_igv){
            $result .= '<span class="igv">IGV incluido*</span>';
        }
        $result .= '</p></div>';

        echo $result;
    }
}

function show_server_wp_product($option)
{
    $id = (int)get_option('server_wp_' . $option);
    $_pf = new WC_Product_Factory();
    $_product = $_pf->get_product($id);
    if ($_product) {
        echo '<span class="text-primary">' . $_product->get_title() . ' </span>- ' . wc_price($_product->get_price()) . ' al mes';
    }
}

// function show_domain_promo_item2($option)
// {
//     $id = (int)get_option('server_wp_' . $option);
//     $_pf = new WC_Product_Factory();
//     $_product = $_pf->get_product($id);
//     if ($_product) {
//         echo '<div class="price-offer col-sm-4"><span class="text-primary">' . $_product->get_title() . ' </span>consíguelo a sólo <p class="prices"><span class="before"> ' . wc_price($_product->get_price()) . '</span><span class="igv">IGV incluido*</span></p></div>';
//     }
// }

function show_product_price($id)
{    
    $_pf = new WC_Product_Factory();
    $_product = $_pf->get_product($id);
    if ($_product) {
        $price = get_rounded_price($_product->get_price());           
        $regular_price = get_rounded_price($_product->get_regular_price());
        $currency_simbol = get_woocommerce_currency_symbol($currency);
        echo $currency_simbol.$price;
    }else{
        echo 'no product found';
    }
}


function get_currency_in_currencies_available()
{
    global $WOOCS;
    $country_code = $WOOCS->storage->get_val('woocs_user_country');
    $currency = $WOOCS->get_currency_by_country($country_code);
    $currencies = $WOOCS->get_currencies();
    $country = $currencies[$currency];
    if ($country) {
        return $country;
    }
    return false;
}

function get_possible_path()
{
    $path = $_SERVER['REQUEST_URI'];
    $urls_parts = explode('/', $path);
    $path = $urls_parts[count($urls_parts) - 2];
    $urls_parts = explode('-', $path);
    $new_path = str_replace('-' . $urls_parts[count($urls_parts) - 1], '', $path);

    if ($new_path == 'aqphost' || $new_path == '/' || $new_path == '') {
        $new_path = 'inicio';
    }
    return $new_path;
}

function redondear_mas($value, $places=0) {
  
  $mult = pow(10, $places);  
  //echo '$value: '.$value. ' $mult'.$mult.' = '.ceil($value/$mult)*$mult;
  return ceil($value/$mult)*$mult;
}

function valor_es_menor_a($a, $b){
    return $a<$b;
}

function show_igv()
{
    global $WOOCS;
    $currencies = $WOOCS->get_currencies();
    $currency = $currencies[$WOOCS->current_currency];
    return $currency['show_igv']==1;
}

function has_offer($product){
    if(is_null($product)){
        return false;
    }
    if($product->get_regular_price() == $product->get_price()){
        return false;
    }
    return true;
}

function get_rounded_price($price)
{
    global $WOOCS;
    $currencies = $WOOCS->get_currencies();
    $currency = $currencies[$WOOCS->current_currency];

    $price_tmp = $price;

    try {
        $round_rule_1 = $currency['round_rule_1'];
        if($round_rule_1){
            eval($round_rule_1);
        }
        if($price_tmp != $price){
            return $price;
        }
    } catch (Exception $e) {
        echo 'Round Rule 1 is invalid';
    }
    try {
        $round_rule_2 = $currency['round_rule_2'];
        if($round_rule_2){
            eval($round_rule_2);
        }
        if($price_tmp != $price){
            //echo '2 >> '.$price;
            return $price;
        }
    } catch (Exception $e) {
        echo 'Round Rule 2 is invalid';
    }
    try {
        $round_rule_3 = $currency['round_rule_3'];
        if($round_rule_3){
            eval($round_rule_3);
        }
        if($price_tmp != $price){
            //echo '3 >> '.$price;
            return $price;
        }
    } catch (Exception $e) {
        echo 'Round Rule 3 is invalid';
    }
    return $price;
}

function get_allowed_countries(){
    $tmp = new WC_Countries();
    return $tmp->get_allowed_countries();
}

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );



function myFunction(){
echo 2;
die();
}
add_action('wp_ajax_myFunction', 'myFunction');
add_action('wp_ajax_nopriv_myFunction', 'myFunction');


function ajax_search_domain_extension(){
    $result = search_domain_extension($_POST['keyword']);
    $json_result = array();
    $currency_simbol = get_woocommerce_currency_symbol($currency);
    foreach ($result as $key => $product){
        $regular_price = $product->get_regular_price();
        $price = get_rounded_price($product->get_price());
        array_push($json_result, array(
            'price' => $price,
            'regular_price' => $regular_price,
            'domain' => $key,
            'has_offer' => has_offer($product),
            'show_igv' => show_igv(),
            'currency_simbol' => $currency_simbol
        )
        );
    }
    header('Content-Type: application/json');
    echo json_encode($json_result);
die();
}

add_action('wp_ajax_searchDomainExtension', 'ajax_search_domain_extension');
add_action('wp_ajax_nopriv_searchDomainExtension', 'ajax_search_domain_extension'); // not really needed

function ajax_search_domain_prefix(){
    $result = search_domain_prefix($_POST['keyword']);
    $json_result = array();
    $currency_simbol = get_woocommerce_currency_symbol($currency);
    foreach ($result as $key => $product){
        $regular_price = $product->get_regular_price();
        $price = get_rounded_price($product->get_price());
        array_push($json_result, array(
            'price' => $price,
            'regular_price' => $regular_price,
            'domain' => $key,
            'has_offer' => has_offer($product),
            'show_igv' => show_igv(),
            'currency_simbol' => $currency_simbol
        )
        );
    }
    header('Content-Type: application/json');
    echo json_encode($json_result);
die();
}

add_action('wp_ajax_searchDomainPrefix', 'ajax_search_domain_prefix');
add_action('wp_ajax_nopriv_searchDomainPrefix', 'ajax_search_domain_prefix'); // not really needed


function ajax_search_domain_coloquial(){
    $result = search_domain_coloquial($_POST['keyword']);
    $json_result = array();
    $currency_simbol = get_woocommerce_currency_symbol($currency);
    foreach ($result as $key => $product){
        $regular_price = $product->get_regular_price();
        $price = get_rounded_price($product->get_price());
        array_push($json_result, array(
            'price' => $price,
            'regular_price' => $regular_price,
            'domain' => $key,
            'has_offer' => has_offer($product),
            'show_igv' => show_igv(),
            'currency_simbol' => $currency_simbol
        )
        );
    }
    header('Content-Type: application/json');
    echo json_encode($json_result);
die();
}

add_action('wp_ajax_searchDomainColoquial', 'ajax_search_domain_coloquial');
add_action('wp_ajax_nopriv_searchDomainColoquial', 'ajax_search_domain_coloquial'); // not really needed